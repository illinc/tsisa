#include<stdlib.h>
#include<iostream.h>
#include<fstream.h>
#include<conio.h>


#ifndef __SMO
#define __SMO
 //enum BOOL {FALSE,TRUE};
class TSmo{

 double  qstream;             //Lambda
 double  loading;             //Roi
 double  input_probablity;    // Pij
 double  delay;               //toi
 double  time_p;
 double  time_w;
   // int devices;             //m
 void  settime_p(){time_p=delay/(1-loading);}
 void  setqstream(int k,double sq) {qstream=k*sq;}
 void  setdelay();
 void  setloading(){loading=qstream*delay;}
 void  settime_w(){time_w=delay*loading/(1-loading);}
public:
	TSmo(){randomize();}
	TSmo(int k,double sq){randomize();Reset(k,sq);}
  void  Reset(int k,double sq);

double  getloading(){return loading;}

double  getqstream() {return qstream;}

double  getdelay(){return delay;}

double  gettime_p(){return time_p;}

double  gettime_w(){return time_w;}

friend ostream& operator<<(ostream& ,TSmo & );

};

class TSystem__{
double  friq[5];
  TSmo  smo[5];
double  qstream;
double  reception;
double  time_p;
double  time_w;
double  delay;
   int  inters;
   int  effic;

  void  setreception();
  void  settime_p();
  void  settime_w();
  void  setdelay(){delay=time_p-time_w;}
public:
     TSystem__(int,int,double);
     TSystem__();
  void  Reset(int,int,double);

double  getreception(){return reception;}

double  gettime_p(){return time_p;}

double  gettime_w(){return time_w;}

double  getdelay(){return delay;}

  TSmo  getsmo(int i){return smo[i-1];}

friend ostream& operator<<(ostream& os,TSystem__& Syst);

};
void TSmo::Reset(int k,double sq)
{
 setqstream(k,sq);
 setdelay();
 setloading();
 settime_p();
 settime_w();
}

void TSmo::setdelay()
{
 double maxdelay=1/qstream;

 while(!(delay=random(maxdelay*100000)));

 delay=delay/100000;
}

ostream& operator<<(ostream& os ,TSmo& smo )
{
 os<<"Lambda: "<<smo.qstream<<'\t'<<"loading: "<<smo.loading<<'\t'
 <<"Delay: "<<smo.delay<<'\t'<<"time_p: "<<smo.time_p;
 return os;
}

//for TSystem
   TSystem__::TSystem__(int effic_,int inter_,double qs)
{
 friq[0]=1000;
 friq[1]=333;
 friq[2]=666;
 friq[4]=1;
 friq[3]=1;
/* inters=inter_;
 effic=effic_;
 qstream=qs;*/
 Reset(effic_,inter_,qs);
 }
   TSystem__::TSystem__()
   {
    friq[0]=1000;
    friq[1]=333;
    friq[2]=666;
    friq[4]=1;
    friq[3]=1;
   }
void TSystem__::Reset(int eff,int inter,double qs )
{
 effic=eff;
 inters=inter;
 qstream=qs;
 for(int i=0;i<5;i++)
 smo[i].Reset(effic*inters,qstream*friq[i]);
 setreception();
 settime_p();
 settime_w();
 setdelay();
}
void TSystem__::setreception()
{
 double maxlo=smo[0].getloading();
 for(int i=1;i<5;i++)
   if (smo[i].getloading()>maxlo) maxlo=smo[i].getloading();
 reception=qstream/maxlo;
}
void TSystem__::settime_p()
{
 double summ=0;
 for(int i=0;i<5;i++)
  summ+=effic*inters*friq[i]*smo[i].gettime_p();
  time_p=summ;
}

void TSystem__::settime_w()
{
 double summ=0;
 for(int i=0;i<5;i++)
  summ+=effic*inters*friq[i]*smo[i].gettime_w();
 time_w=summ;
}
ostream& operator<<(ostream& os, TSystem__& Syst)
{
os<<endl<<"Lambda: "<<Syst.qstream<<'\t'
      /*	 <<"loading: "<<Syst.loading<<endl */
	 <<"reciption c..: "<<Syst.reception<<'\t'
	 <<"time_p: "<<Syst.time_p;
return os;
	 }

#endif

