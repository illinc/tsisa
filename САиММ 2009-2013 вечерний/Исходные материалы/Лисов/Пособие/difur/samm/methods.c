#include	<conio.h>
#include	<stdlib.h>
#include	<string.h>
#include	"main.h"
#include	"func.h"

int n, store_n;
float *main_point;
float *store_point;
float t;

int check_input (void)
{
	qtext();
	clrscr();
	if (rang < 0) {
		gotoxy (25, 12);
		cputs ("���砫� ������ ��⥬� �p�������");
		getch();
		return 0;
	}
	if (t0 < 0) {
		gotoxy (25, 12);
		cputs ("���砫� ������ ��砫�� �᫮���");
		getch();
		return 0;
	}
	if (h < 0) {
		gotoxy (25, 12);
		cputs ("���砫� ������ ��p����p� ���p��ᨬ�樨");
		getch();
		return 0;
	}
	return 1;
}

int get_mem (void)
{
	n = (b-a)/h+1;
	if ((n > (65535u/rang/sizeof(float))) || (n < 0)) {
		gotoxy (25, 12);
		cputs ("���誮� ������ p��������");
		getch();
		return 0;
	}
	return 1;
}

float x[5];
float newx[5];

static int pc;
void copy_res (void)
{
int i;
static int cur_pc;
	for (i=0; i<rang; i++) main_point[rang*n+i] = x[i];
	n ++;
	cur_pc = (int)((t-a)/(b-a)*100);
	if (pc == cur_pc) return;
	gotoxy (33, 20);
	cprintf ("%2d", cur_pc);
}

void init_x (void)
{
int i;
	gotoxy (32, 12);
	cputs ("Calculating ...");
	gotoxy (33, 20);
	cputs (" 0% Complete.");
	for (i=0; i<rang; i++) x[i] = x0[i];
	pc = 0;
}

void copy_new (void)
{
int i;
	for (i=0; i<rang; i++) x[i] = newx[i];
}

void eyler1 (void)
{
int i;
	if (!check_input()) return;
	if (!get_mem()) return;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * f(i, t, x);
		}
		copy_new();
		t += h;
	}
}

void rk2 (void)
{
int i;
float xw[5];
	if (!check_input()) return;
	if (!get_mem()) return;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) xw[i] = x[i] + h * f (i, t, x) / 2;
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * f(i, t+h/2, xw);
		}
		copy_new();
		t += h;
	}
}

void rk3 (void)
{
int i;
float q1[5];
float q2[5];
float q3[5];
	if (!check_input()) return;
	if (!get_mem()) return;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) q1[i] = h * f (i, t, x);
		for (i=0; i<rang; i++) q2[i] = x[i] + q1[i] / 2;
		for (i=0; i<rang; i++) q2[i] = h * f (i, t+h, q2);
		for (i=0; i<rang; i++) q3[i] = x[i] - q1[i] + 2 * q2[i];
		for (i=0; i<rang; i++) q3[i] = h * f (i, t+h, q3);
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + (q1[i] + 4 * q2[i] + q3[i]) / 6;
		}
		copy_new();
		t += h;
	}
}

void rk4 (void)
{
int i;
float q1[5];
float q2[5];
float q3[5];
float q4[5];
	if (!check_input()) return;
	if (!get_mem()) return;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) q1[i] = h * f (i, t, x);
		for (i=0; i<rang; i++) q2[i] = x[i] + q1[i] / 3;
		for (i=0; i<rang; i++) q2[i] = h * f (i, t+h/3, q2);
		for (i=0; i<rang; i++) q3[i] = x[i] - q1[i] / 3 + q2[i];
		for (i=0; i<rang; i++) q3[i] = h * f (i, t+2/3*h, q3);
		for (i=0; i<rang; i++) q4[i] = x[i] - q1[i] - q2[i] + q3[i];
		for (i=0; i<rang; i++) q4[i] = h * f (i, t+h, q4);
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + (q1[i] + 3 * q2[i] + 3 * q3[i] + q4[i]) / 8;
		}
		copy_new();
		t += h;
	}
}

static int first (int k)
{
int i;
float q1[5];
float q2[5];
float q3[5];
float q4[5];
	if (!check_input()) return 0;
	if (!get_mem()) return 0;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		if (n > k) goto OK;
		for (i=0; i<rang; i++) q1[i] = h * f (i, t, x);
		for (i=0; i<rang; i++) q2[i] = x[i] + q1[i] / 3;
		for (i=0; i<rang; i++) q2[i] = h * f (i, t+h/3, q2);
		for (i=0; i<rang; i++) q3[i] = x[i] - q1[i] / 3 + q2[i];
		for (i=0; i<rang; i++) q3[i] = h * f (i, t+2/3*h, q3);
		for (i=0; i<rang; i++) q4[i] = x[i] - q1[i] - q2[i] + q3[i];
		for (i=0; i<rang; i++) q4[i] = h * f (i, t+h, q4);
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + (q1[i] + 3 * q2[i] + 3 * q3[i] + q4[i]) / 8;
		}
		copy_new();
		t += h;
	}
	return 0;
OK:
	return 1;
}

void ab2 (void)
{
int i;
	if (!first (2)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * (1.5 * f (i, t, x) -
									.5 * f (i, t-h, main_point+rang*(n-2)));
		}
		copy_new();
		copy_res();
		t += h;
	}
}

void ab3 (void)
{
int i;
	if (!first (3)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * (23.0/12.0 * f (i, t, x) -
						16.0/12.0 * f (i, t-h, main_point+rang*(n-2)) +
						5.0/12.0 * f (i, t-h-h, main_point+rang*(n-3)));
		}
		copy_new();
		copy_res();
		t += h;
	}
}

void ab4 (void)
{
int i;
	if (!first (4)) return;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * (55.0/24.0 * f (i, t, x) -
						59.0/24.0 * f (i, t-h, main_point+rang*(n-2)) +
						37.0/24.0 * f (i, t-h-h, main_point+rang*(n-3)) -
						9.0/24.0 * f (i, t-h*3, main_point+rang*(n-4)));
		}
		copy_new();
		t += h;
	}
}

void ab5 (void)
{
int i;
	if (!first (5)) return;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * (1901.0/720.0 * f (i, t, x) -
						2774.0/720.0 * f (i, t-h, main_point+rang*(n-2)) +
						2616.0/720.0 * f (i, t-h-h, main_point+rang*(n-3)) -
						1274.0/720.0 * f (i, t-h*3, main_point+rang*(n-4)) +
						251.0/720.0 * f (i, t-h*4, main_point+rang*(n-5)));
		}
		copy_new();
		t += h;
	}
}

void ab6 (void)
{
int i;
	if (!first (6)) return;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) {
			newx[i] = x[i] + h * (4277.0/1440.0 * f (i, t, x) -
						7923.0/1440.0 * f (i, t-h, main_point+rang*(n-2)) +
						9982.0/1440.0 * f (i, t-h-h, main_point+rang*(n-3)) -
						7298.0/1440.0 * f (i, t-h*3, main_point+rang*(n-4)) +
						2877.0/1440.0 * f (i, t-h*4, main_point+rang*(n-5)) -
						475.0/1440.0 * f (i, t-h*5, main_point+rang*(n-6)));
		}
		copy_new();
		t += h;
	}
}

void am1 (void)
{
int i;
float nx[5];
	if (!check_input()) return;
	if (!get_mem()) return;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++) newx[i] = x[i] + h * f(i, t+h, nx);
		copy_new();
		t += h;
	}
}

void am2 (void)
{
int i;
float nx[5];
	if (!check_input()) return;
	if (!get_mem()) return;
	init_x();
	t = a;
	n = 0;
	while (t <= b) {
		copy_res();
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = x[i] + h * (.5 * f (i, t+h, nx) + .5 * f (i, t, x));
		copy_new();
		t += h;
	}
}

void am3 (void)
{
int i;
float nx[5];
	if (!first (1)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = x[i] + h * (5.0/12.0 * f (i, t+h, nx) +
						8.0/12.0 * f (i, t, x) -
						1.0/12.0 * f (i, t-h, main_point+rang*(n-2)));
		copy_new();
		t += h;
		copy_res();
	}
}

void am4 (void)
{
int i;
float nx[5];
	if (!first (2)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = x[i] + h * (9.0/24.0 * f (i, t+h, nx) +
						19.0/24.0 * f (i, t, x) -
						5.0/24.0 * f (i, t-h, main_point+rang*(n-2)) +
						1.0/24.0 * f (i, t-h-h, main_point+rang*(n-3)));
		copy_new();
		t += h;
		copy_res();
	}
}

void am5 (void)
{
int i;
float nx[5];
	if (!first (3)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = x[i] + h * (251.0/720.0 * f (i, t+h, nx) +
						646.0/720.0 * f (i, t, x) -
						264.0/720.0 * f (i, t-h, main_point+rang*(n-2)) +
						106.0/720.0 * f (i, t-h-h, main_point+rang*(n-3)) -
						19.0/720.0 * f (i, t-h*3, main_point+rang*(n-4)));
		copy_new();
		t += h;
		copy_res();
	}
}

void am6 (void)
{
int i;
float nx[5];
	if (!first (4)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = x[i] + h * (475.0/1440.0 * f (i, t+h, nx) +
						1427.0/1440.0 * f (i, t, x) -
						798.0/1440.0 * f (i, t-h, main_point+rang*(n-2)) +
						482.0/1440.0 * f (i, t-h-h, main_point+rang*(n-3)) -
						173.0/1440.0 * f (i, t-h*3, main_point+rang*(n-4)) +
						27.0/1440.0 * f (i, t-h*4, main_point+rang*(n-5)));
		copy_new();
		t += h;
		copy_res();
	}
}

void g2 (void)
{
int i;
float nx[5];
	if (!first (1)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = 4.0/3.0 * x[i] -
						1.0/3.0 * main_point[rang*(n-2)+i] +
						h * 2.0/3.0 * f(i, t+h, nx);
		copy_new();
		t += h;
		copy_res();
	}
}

void g3 (void)
{
int i;
float nx[5];
	if (!first (2)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = 18.0/11.0 * x[i] -
						9.0/11.0 * main_point[rang*(n-2)+i] +
						2.0/11.0 * main_point[rang*(n-3)+i] +
						h * 6.0/11.0 * f(i, t+h, nx);
		copy_new();
		t += h;
		copy_res();
	}
}

void g4 (void)
{
int i;
float nx[5];
	if (!first (3)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = 48.0/25.0 * x[i] -
						36.0/25.0 * main_point[rang*(n-2)+i] +
						16.0/25.0 * main_point[rang*(n-3)+i] -
						3.0/25.0 * main_point[rang*(n-4)+i] +
						h * 12.0/25.0 * f(i, t+h, nx);
		copy_new();
		t += h;
		copy_res();
	}
}

void g5 (void)
{
int i;
float nx[5];
	if (!first (4)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = 300.0/137.0 * x[i] -
						300.0/137.0 * main_point[rang*(n-2)+i] +
						200.0/137.0 * main_point[rang*(n-3)+i] -
						75.0/137.0 * main_point[rang*(n-4)+i] +
						12.0/137.0 * main_point[rang*(n-5)+i] +
						h * 60.0/137.0 * f(i, t+h, nx);
		copy_new();
		t += h;
		copy_res();
	}
}

void g6 (void)
{
int i;
float nx[5];
	if (!first (5)) return;
	while (t <= b) {
		for (i=0; i<rang; i++) nx[i] = x[i] + h * f(i, t, x);
		for (i=0; i<rang; i++)
			newx[i] = 360.0/147.0 * x[i] -
						450.0/147.0 * main_point[rang*(n-2)+i] +
						400.0/147.0 * main_point[rang*(n-3)+i] -
						225.0/147.0 * main_point[rang*(n-4)+i] +
						72.0/147.0 * main_point[rang*(n-5)+i] -
						10.0/147.0 * main_point[rang*(n-5)+i] +
						h * 60.0/147.0 * f(i, t+h, nx);
		copy_new();
		t += h;
		copy_res();
	}
}
