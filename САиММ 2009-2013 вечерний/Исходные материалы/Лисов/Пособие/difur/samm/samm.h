
typedef enum {
	NONE,
	CALL_FUNC,
	ANOTHER_MENU,
	MAIN_MENU
} MENU_COMMANDS;

typedef struct __Q__{
	char name [35];
	MENU_COMMANDS command;
	void (*func) (void);
	struct __Q__ * new;
} MENU_ITEM;

#define		END_OF_MENU		{ "", NULL, NULL, NULL}

#define		CR			13
#define		SPACE		32
#define		BS			8
#define		CtrlZ		26

#define		UP_KEY			72
#define		DOWN_KEY		80
#define		LEFT_KEY		75
#define		RIGHT_KEY		77
#define		DEL_KEY			83
#define		PGDN_KEY		81
#define		PGUP_KEY		73
#define		CTRLPGUP_KEY	132
#define		CTRLPGDN_KEY	118
#define		CTRLHOME_KEY	119
#define		CTRLEND_KEY		117
#define		HOME_KEY		71
#define		END_KEY			79
#define		ESC_KEY			27

