#include	<string.h>
#include	<stdlib.h>
#include	<ctype.h>
#include	<math.h>
#include	"main.h"

enum {
	PAR_PRIOR,
	PM_PRIOR,
	MD_PRIOR,
	POWER_PRIOR,
	FUNC_PRIOR
};

#define		FUNC_BOUND		140

#define		VAL_PREFIX		130
#define		X_PREFIX		131

int spos, dpos, ops, sn;
char oper_stack[40];
char *source;
char *dest;

static char f_names[][3] = {
	"EXP",
	"SIN",
	"COS",
	"TAN",
	"ABS",
	"LOG",

};

enum {
	F_EXP = FUNC_BOUND,
	F_SIN,
	F_COS,
	F_TAN,
	F_ABS,
	F_LOG,
	UNARY_MINUS	= 170
};

#define     NUM_FUNC	(sizeof(f_names)/3)

int check_func (char *s)
{
int i, j;
char ch;
	sn = 0;
	j = 0;
	for (i=0;;i++) {
		ch = s[i];
		if (ch == '\0') break;
		if (ch == '(') j++;
		if (ch == ')') j--;
		if (j < 0) return 0;
	}
	if (j != 0) return 0;
	for (i=0; s[i]!='\0';) {
		ch = s[i];
		if (isalpha (ch)) {
			for (j=0; j<NUM_FUNC; j++) {
				if (memcmp (s+i, f_names[j], 3) == 0) goto found;
			}
			if (ch == 'X') {
				s[i] = X_PREFIX;
				i+=2;
				continue;
			}
			if (ch == 'T') {
				i++;
				continue;
			}
			return 0;
found:
			s[i++] = j + FUNC_BOUND;
			for (j=i; ; j++) {
				s[j] = s[j+2];
				if (s[j] == '\0') break;
			}
		} else i++;
	}
/* Find UNARY -		*/
	i = 0;
	for (;;) {
		if (s[i] == '-') s[i] = UNARY_MINUS;
		while ((s[i] != '\0') && (s[i] != '(')) i++;
		if (s[i] == '\0') break;
		i++;
	}
	sn = strlen (s);
	ch = s[sn-1];
	return (isdigit(ch) || (ch == 'T') || (ch == ')'));
}

int prior (char ch)
{
	if (ch >= FUNC_BOUND) return FUNC_PRIOR;
	if ((ch == '-') || (ch == '+')) return PM_PRIOR;
	if ((ch == '*') || (ch == '/')) return MD_PRIOR;
	if (ch == '(') return PAR_PRIOR;
	if (ch == '^') return POWER_PRIOR;
	return -1;
}

void to_stack (void)
{
	oper_stack[ops++] = source[spos++];
}

static char buf[20];
void copy_digit (void)
{
char ch;
int i=0;
float ftmp;
	ch = source[spos];
	while ((isdigit(ch)) || (ch == '.')) {
		buf[i++] = ch;
		ch = source[++spos];
	}
	buf[i] = '\0';
	ftmp = atof (buf);
	dest[dpos++] = VAL_PREFIX;
	*((float*)(dest+dpos)) = ftmp;
	dpos += sizeof (float);
}

void copy_x (void)
{
	spos++;
	dest[dpos++] = X_PREFIX;
	dest[dpos++] = source[spos++] - '1';
}

int translate (char *s, char *d)
{
char ch;
int pr;
	source = s;
	dest = d;
	strupr (source);
	if (!check_func (source)) return 0;
	ops = 0;
	spos = 0;
	dpos = 0;
	while (spos < sn) {
		ch = source[spos];
		if ((ch >= FUNC_BOUND) || (ch == '(')) {
			to_stack();
			continue;
		} else
		if (isdigit (ch) || (ch == '.')) {
			copy_digit();
			continue;
		} else
		if (ch == X_PREFIX) {
			copy_x();
			if ((unsigned)dest[dpos-1] >= rang) return 0;
			continue;
		} else
		if (ch == 'T') {
			dest[dpos++] = source[spos++];
		} else
		if (ch == ')') {
			spos ++;
			while (ops > 0) {
				ch = oper_stack [--ops];
				if (ch=='(') break;
				dest[dpos++] = ch;
			}
		} else
		if ((ch=='+') || (ch=='-') || (ch=='*') || (ch=='/') ||	(ch=='^')) {
			pr = prior (ch);
			while (ops > 0) {
				ch = oper_stack [--ops];
				if (prior (ch) > pr) dest[dpos++] = ch;
				else {
					ops++;
					break;
				}
			}
			to_stack();
		} else return 0;
	}
	while (ops > 0) dest[dpos++] = oper_stack[--ops];
	return dpos;
}

float f (int i, float t, float * x)
{
static float stack[40];
int sp;
char ch;
int len;
char *fun;
	len = len_funcs[i];
	fun = funcs[i];
	sp = 0;
	i = 0;
	while (i<len) {
		ch = fun[i++];
		if (ch == X_PREFIX) {
			ch = fun[i++];
			stack[sp++] = x[ch];
		}
		if (ch == 'T') {
			stack[sp++] = t;
		}
		if (ch == '+') {
			sp -= 1;
			stack[sp-1] += stack[sp];
		}
		if (ch == '-') {
			sp -= 1;
			stack[sp-1] -= stack[sp];
		}
		if (ch == '*') {
			sp -= 1;
			stack[sp-1] *= stack[sp];
		}
		if (ch == '/') {
			sp -= 1;
			stack[sp-1] /= stack[sp];
		}
		if (ch == '^') {
			sp -= 1;
			stack[sp-1] = pow (stack[sp-1], stack[sp]);
		}
		if (ch == VAL_PREFIX) {
			stack[sp++] = *((float*)(fun+i));
			i += sizeof(float);
		}
		if (ch >= FUNC_BOUND) {
			switch (ch) {
				case F_EXP:
					stack[sp-1] = exp (stack[sp-1]);
					break;
				case F_SIN:
					stack[sp-1] = sin (stack[sp-1]);
					break;
				case F_COS:
					stack[sp-1] = cos (stack[sp-1]);
					break;
				case F_TAN:
					stack[sp-1] = tan (stack[sp-1]);
					break;
				case F_ABS:
					stack[sp-1] = fabs (stack[sp-1]);
					break;
				case F_LOG:
					stack[sp-1] = log (stack[sp-1]);
					break;
				case UNARY_MINUS:
					stack[sp-1] = -stack[sp-1];
					break;
			}
		}
	}
	return stack[0];
}
