﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Runtime.Serialization.Formatters.Soap;
using System.IO;

namespace Gene2
{
    class GeneTable
    {
        public static readonly Random rnd = new Random();


        public static readonly int a;
        public static readonly int b;

        static GeneTable()
        {
            a = 3;
            b = 10;
            K = 2;
            N = 3;
            M = N * 2;
        }

        int[,] table = new int[a, b];

        public int this[int i, int j]
        {
            get
            {
                return table[i, j];
            }
        }

        public List<Chromosome> Population
        {
            get
            {
                return population;
            }
        }


        public GeneTable(string file)
        {
            SoapFormatter sf = new SoapFormatter();

            FileStream fs = new FileStream(file, FileMode.Open);

            table = sf.Deserialize(fs) as int[,];

            fs.Close();

            mkPopulation();
        
        }

        public GeneTable()
        {
            for (int i = 0; i < a; ++i )
                for (int j = 0; j < b; ++j)
                    if (rnd.Next(3) == 0 && ((i != 0 && table[i - 1, j] != -1) || i == 0))
                        table[i, j] = -1;
                    else
                        table[i, j] = rnd.Next(1, 11);



            mkPopulation();


            //SoapFormatter sf = new SoapFormatter();
            //sf.Serialize(new FileStream("table1.xml", FileMode.Create), table);


            //foreach (Chromosome c in population)
            //    c.Show();

            //Console.BackgroundColor = ConsoleColor.Red;
            //Console.ForegroundColor = ConsoleColor.Yellow;

            //for (; ; )
            //{
            //    Console.Clear();
            //    foreach (Chromosome c in population)
            //        c.Show();



            //    mkProdac();

            //    mkChildren();


            //    newPopulation();
            //}



            //for (int i = 0; i < b; ++i)
            //{
            //    for (int j = 0; j < a; ++j)
            //        Console.Write("{0,3} ", table[j, i]);
            //    Console.WriteLine();
            //}





        }

        public static readonly int N;
 //       Chromosome[] chroms = new Chromosome[N];
        List<Chromosome> population = new List<Chromosome>(N);
        
        public void mkPopulation()
        {
            for (int i = 0; i < N; ++i)
                population.Add(new Chromosome(this));
        }

        public void newPopulation()
        { 
            population.Clear();
            children.Sort();

            for (int i = 0; i < N; ++i)
                population.Add(children[i]);

//            population.Add(children[M - 1]);

            population.Sort();


        }


        static readonly int K;
        Chromosome[] prodac = new Chromosome[K];

        internal Chromosome[] Prodac
        {
            get
            {
                return prodac; 
            }
        }

        public void mkProdac()
        {
            population.Sort();
            for (int i = 0; i < K; ++i)
                prodac[i] = population[i];
        }

        static readonly int M;
        List<Chromosome> children = new List<Chromosome>(M);

        internal List<Chromosome> Children
        {
            get 
            { 
                return children; 
            }
        }
        public void mkChildren()
        {
            children.Clear();

            foreach (Chromosome ch in prodac)
            {
                children.Add(ch);
                children.Add(ch.Mutate());
            }

            int c = children.Count;
            for (int i = 0; i < M - c; ++i)
            {
                Chromosome a = prodac[GeneTable.rnd.Next(prodac.Length)];
                Chromosome b;
                do
                {
                    b = prodac[GeneTable.rnd.Next(prodac.Length)];
                }
                while (a == b);

                children.Add(a + b);
            
            }



        }





    }


    class Chromosome : IComparable, IEnumerable
    {


        int[] chrom = new int[GeneTable.b];
        GeneTable gt;

        public int this[int i]
        {
            get 
            {
                return chrom[i];
            }
        }

        public Chromosome(GeneTable table)
        {
            gt = table;
            for (int i = 0; i < GeneTable.b; ++i)
                do
                    chrom[i] = GeneTable.rnd.Next(0, GeneTable.a);
                while (gt[chrom[i], i] == -1);

            CountG();
        }

        private Chromosome()
        { 
        }

        public static Chromosome operator +(Chromosome ch1, Chromosome ch2)
        {
            Chromosome result = new Chromosome();
            result.gt = ch1.gt;

            int c = GeneTable.rnd.Next(0, 9);

            for (int i = 0; i < GeneTable.b; ++i)
                if (i <= c)
                    result.chrom[i] = ch1[i];
                else
                    result.chrom[i] = ch2[i];

            result.CountG();
            return result;
        }

        public Chromosome Mutate()
        { 
            Chromosome ch = new Chromosome();
            ch.gt = gt;
            ch.chrom = (int[])chrom.Clone();

            int a;
            int b;
            do
            {
                b = GeneTable.rnd.Next(GeneTable.b);
                a = GeneTable.rnd.Next(GeneTable.b);
            }
            while (gt[chrom[a], b] == -1 || gt[chrom[b], a] == -1 || a == b);

            ch.chrom[a] += ch[b];
            ch.chrom[b] = ch[a] - ch[b];
            ch.chrom[a] -= ch[b];


            //int m = GeneTable.rnd.Next(GeneTable.b);
            //int v;
            //do
            //{
            //    v = GeneTable.rnd.Next(GeneTable.a);
            //}
            //while (gt[v, m] == -1);

            //ch.chrom[m] = v;


            ch.CountG();

            return ch;
        }

        void CountG()
        {

            g = 0;
            for (int i = 0; i < chrom.Length; ++i)
                g += gt[chrom[i], i];

        }

        int g;

        public int G
        {
            get 
            { 
                return g;
            }
        }




        int IComparable.CompareTo(object obj)
        {
            return g.CompareTo((obj as Chromosome).g);
        }

        public void Show()
        {
            for(int i = 0; i < 20; ++i)
                Console.Write("{0,3} ", gt[chrom[i], i]);

            Console.WriteLine();
        
        }



        public IEnumerator GetEnumerator()
        {
            return chrom.GetEnumerator();
        }

    }
}
