﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Soap;
using System.IO;

namespace Gene2
{
    
    public partial class Form1 : Form
    {

        GeneTable table;// = new GeneTable("table1.xml");

        

        public Form1()
        {
            InitializeComponent();
            

 //           DoTable("table1.xml");
        }

        void DoTable(string name)
        {
            step = 0;
            state = GeneState.Step1;
            richTextBox1.Clear();
            if (name == String.Empty)
                table = new GeneTable();
            else
                table = new GeneTable(name);


            listView1.Items.Clear();

            for (int i = 0; i < GeneTable.b; ++i)
            {
                string[] items = new string[GeneTable.a + 1];
                items[0] = "№ " + (i + 1).ToString();

                for (int j = 0; j < GeneTable.a; ++j)
                    if (table[j, i] != -1)
                        items[j + 1] = table[j, i].ToString();
                    else
                        items[j + 1] = String.Empty;

                listView1.Items.Add(new ListViewItem(items));
            }

            button1.Visible = true;
            button1.Text = "Шаг 1";
            label1.Text = "Генерация исходной популяции случайным образом (N = 3).";
 //           ShowCroms(table.Population);

        }


        void ShowCroms(IEnumerable<Chromosome> array)
        {
            foreach(Chromosome c in array)
            {
                foreach (int i in c)
                    richTextBox2.Text += String.Format("{0,4}", i + 1);

                richTextBox2.Text += "\n";//String.Format("  G = {0}\n", c.G);

            }
        
        }


        int step;

        private void button1_Click(object sender, EventArgs e)
        {
            ++step;
            DoStep();
        }

        void DoStep()
        {
            switch (state)
            { 
                case GeneState.Step1:
                    richTextBox2.Text = String.Format("\t\tШаг {0}\nИзначальная популяция:\n", step);
                    ShowCroms(table.Population);
                    richTextBox2.Text += "----------------------------------------------------\n";

                    state++;
                    button1.Text = "Шаг " + (step + 1).ToString(); ;
                    label1.Text = "Оценка качества (приспособленности) особей.";
                    break;

                case GeneState.Step2:
                    richTextBox2.Text =  String.Format("\t\tШаг {0}\nКачество особей (суммарное время работы станков):\n", 
                        step);
                    int i = 1;
                    foreach (Chromosome c in table.Population)
                    {
                        richTextBox2.Text += String.Format("t{0} = {1,2}; ", i, c.G);
                        ++i;
                    }
                    richTextBox2.Text += "\n----------------------------------------------------\n";

                    if (table.Population[0].G == table.Population[GeneTable.N - 1].G && 
                        DialogResult.Yes == MessageBox.Show("Условие остановки алгоритма (tmax = tmin) выполнено.\n" +
                        "Следует ли остановить алгоритм?", "Остановка алгоритма", MessageBoxButtons.YesNo))
                    {
                        richTextBox1.Text += richTextBox2.Text;
  //                      button1.Text = String.Empty;
                        button1.Visible = false;
 //                       listView1.Items.Clear();
                        richTextBox2.Clear();
                        label1.Text = String.Empty;
                        ShowOptimal();
                        break;
                    }

                    state++; 
                    button1.Text = "Шаг " +  (1 + step).ToString();
                    label1.Text = "Пораждение продакционной группы из существующей\n" + 
                        "популяции путём выбора двух наиболее приспособленных особей.";
                    break;

                case GeneState.Step3:
                    richTextBox2.Text = String.Format("\t\tШаг {0}\nПродакционная группа:\n", step);
                    table.mkProdac();
                    ShowCroms(table.Prodac);
                    richTextBox2.Text += "----------------------------------------------------\n";

                    state++;
                    button1.Text = "Шаг " + (step + 1).ToString();
                    label1.Text = "Пораждение потомков по следующему алгоритму:\n" + 
                        "- оба родителя сохраняются;\n" + 
                        "- добавляются две особи, полученные случайной перестановкой двух генов у родителей;\n" +
                        "- добавляются две особи, полученные путём скрещевания (кроссинговера) родителей.";
                    break;

                case GeneState.Step4:
                    richTextBox2.Text =  String.Format("\t\tШаг {0}\nПотомки:\n", step);
                    table.mkChildren();
                    ShowCroms(table.Children);
                    richTextBox2.Text += "----------------------------------------------------\n";

                    ++state;
                    button1.Text = "Шаг " + (step + 1).ToString();
                    label1.Text = "Формирование новой популяции путём выбора наиболее приспособленных потомков.";

                    break;

                case GeneState.Step5:
                    richTextBox2.Text =  String.Format("\t\tШаг {0}\nОценка потомков:\n", step);

                    i = 1;
                    foreach (Chromosome c in table.Children)
                    {
                        richTextBox2.Text += String.Format("t{0} = {1,2}; ", i, c.G);
                        ++i;
                    }
                    richTextBox2.Text += "\n----------------------------------------------------\nНовая популяция:\n";
                    table.newPopulation();
                    ShowCroms(table.Population);
                    richTextBox2.Text += "----------------------------------------------------\n";

                    state = GeneState.Step2;
                    button1.Text = "Шаг " + (step + 1).ToString();
                    label1.Text = "Следующая итерация гннетического алгоритма оптемизации системы.";




                    break;
            }

            richTextBox1.Text += richTextBox2.Text;
        }

        void ShowOptimal()
        {
            richTextBox1.Text += "\n\tНайденное решение:\n";

            int i = 0;
            foreach (int n in table.Population[0])
            {
                richTextBox1.Text += String.Format("Деталь №{0, 2} выполняется на {1}-м станке за {2} ед.\n",
                    i + 1, table.Population[0][i] + 1, table[table.Population[0][i], i]);
                ++i;

            }
        }

        GeneState state;

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void случайнаяТаблицаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoTable(String.Empty);
        }

        private void изФайлаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoTable("table1.xml");
        }


    }

    enum GeneState
    { 
        Step1,
        Step2,
        Step3,
        Step4,
        Step5
    }

}