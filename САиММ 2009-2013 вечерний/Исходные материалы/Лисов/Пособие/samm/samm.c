#include	<conio.h>
#include	<stdlib.h>
#include	<string.h>
#include	"samm.h"
#include	"methods.h"
#include	"graph.h"
#include	"func.h"

#define		MENU_X	30
#define		NORM_COLOR		YELLOW
#define		NORM_BACKGROUND	BLUE
#define		HIGH_COLOR		BLACK
#define		HIGH_BACKGROUND	YELLOW
#define		Q_COLOR			LIGHTCYAN
#define		Q_BACKGROUND	BLUE

#define		MAXSTR		30

MENU_ITEM * cur_menu;
char strbuf[MAXSTR+1];
int rang;
float a, b, h, t0;
float x0[5];
char funcs[5][200];
int len_funcs[5];

void getstring (void)
{
int i = 0, ch;
	for (;;) {
		ch = getch();
		if (ch == 0) { getch(); continue; }
		if (ch < SPACE)
			switch (ch) {
				case CR:	if (i > 0) goto end;
							else break;
				case BS:	if (i <= 0) break;
							gotoxy (wherex()-1, wherey());
							putch (' ');
							gotoxy (wherex()-1, wherey());
							i--;
							break;
			}
		else
			if (i >= MAXSTR - 1) putch (7);
			else {
				ch &= 0x7F;
				strbuf[i++] = ch;
				putch (ch);
			}
	}
end:
	strbuf[i] = '\0';
}

void cls (void)
{
	textbackground (NORM_BACKGROUND);
	clrscr();
}

void end_of_work (void)
{
	free (store_point);
	free (main_point);
	textbackground (BLACK);
	textcolor (LIGHTGRAY);
	clrscr();
	exit (0);
}

void qtext (void)
{
	textcolor (Q_COLOR);
	textbackground (Q_BACKGROUND);
}

int get_int (int x, int y, char *string, int min, int max)
{
int val;
again:
	gotoxy (x, y);
	cputs (string); clreol();
	getstring();
	val = atoi (strbuf);
	if ((val < min) || (val > max)) goto again;
	return val;
}

float get_float (int x, int y, char *string)
{
float val;
again:
	gotoxy (x, y);
	cputs (string); clreol();
	getstring();
	val = atof (strbuf);
/*	if ((rang < min) || (rang > max)) goto again;*/
	return val;
}

void get_system (void)
{
int i;
	qtext();
	clrscr();
	t0 = -1;
	n = -1;
	rang = get_int (10, 5, "������ ���冷� ��⥬� (1..5): ", 1, 5);
	for (i=0; i<rang; i++) {
again:
		gotoxy (13, i+7);
		cprintf ("X%1d' = ", i+1); clreol();
		getstring();
		if ((len_funcs[i] = translate (strbuf, funcs[i])) == 0) goto again;
	}
	n = store_n = -1;
}

void get_param (void)
{
	qtext();
	clrscr();
	a = get_float (10, 5, "�����  �࠭��: ");
	b = get_float (10, 7, "�ࠢ�� �࠭��: ");
	h = get_float (10, 9, "���:            ");
	n = store_n = -1;
}

void get_nach (void)
{
int i;
	qtext();
	clrscr();
	if (rang < 0) {
		gotoxy (25, 12);
		cputs ("���砫� ������ ��⥬� �p�������");
		getch();
		return;
	}
	gotoxy (8, 5);
	cputs ("��砫�� �᫮���: X(A) = X0");
/*	t0 = get_float (10, 7, "t0 = "); */
	for (i=1; i<=rang; i++) {
		strcpy (strbuf, "X0(0) = ");
		strbuf[3] = i + '0';
		x0[i-1] = get_float (10, i+7, strbuf);
	}
	t0 = 1;
	n = store_n = -1;
}

/* Description of menu */
MENU_ITEM metrk[] = {
	{ "�㭣� - ���� 1-�� ���浪�", CALL_FUNC, eyler1, NULL},
	{ "�㭣� - ���� 2-�� ���浪�", CALL_FUNC, rk2, NULL},
	{ "�㭣� - ���� 3-�� ���浪�", CALL_FUNC, rk3, NULL},
	{ "�㭣� - ���� 4-�� ���浪�", CALL_FUNC, rk4, NULL},
	{ "������ � �᭮���� ����   ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM metab[] = {
	{ "����� ������ 1-�� ���浪�", CALL_FUNC, eyler1, NULL},
	{ "����� ������ 2-�� ���浪�", CALL_FUNC, ab2, NULL},
	{ "����� ������ 3-�� ���浪�", CALL_FUNC, ab3, NULL},
	{ "����� ������ 4-�� ���浪�", CALL_FUNC, ab4, NULL},
	{ "����� ������ 5-�� ���浪�", CALL_FUNC, ab5, NULL},
	{ "����� ������ 6-�� ���浪�", CALL_FUNC, ab6, NULL},
	{ "������ � �᭮���� ����    ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM met1[] = {
	{ "��⮤� �㭣� - ����    ", ANOTHER_MENU, NULL, metrk},
	{ "��⮤� ����� - ������", ANOTHER_MENU, NULL, metab},
/*	{ "��⮤ �����             ", NONE, NULL, NULL},	*/
	{ "������ � �᭮���� ���� ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM metam[] = {
	{ "����� - �a�⮭� 1-�� ���浪�", CALL_FUNC, am1, NULL},
	{ "����� - �a�⮭� 2-�� ���浪�", CALL_FUNC, am2, NULL},
	{ "����� - �a�⮭� 3-�� ���浪�", CALL_FUNC, am3, NULL},
	{ "����� - �a�⮭� 4-�� ���浪�", CALL_FUNC, am4, NULL},
	{ "����� - �a�⮭� 5-�� ���浪�", CALL_FUNC, am5, NULL},
	{ "����� - �a�⮭� 6-�� ���浪�", CALL_FUNC, am6, NULL},
	{ "������ � �᭮���� ����       ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM metgir[] = {
	{ "��⮤ ��� 1-�� ���浪�", CALL_FUNC, am1, NULL},
	{ "��⮤ ��� 2-�� ���浪�", CALL_FUNC, g2, NULL},
	{ "��⮤ ��� 3-�� ���浪�", CALL_FUNC, g3, NULL},
	{ "��⮤ ��� 4-�� ���浪�", CALL_FUNC, g4, NULL},
	{ "��⮤ ��� 5-�� ���浪�", CALL_FUNC, g5, NULL},
	{ "��⮤ ��� 6-�� ���浪�", CALL_FUNC, g6, NULL},
	{ "������ � �᭮���� ����", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM met2[] = {
	{ "��⮤� ����� - �a�⮭�", ANOTHER_MENU, NULL, metam},
	{ "��⮤� ���             ", ANOTHER_MENU, NULL, metgir},
	{ "������ � �᭮���� ���� ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM task_menu[] = {
	{ "���� ��⥬� �ࠢ�����       ", CALL_FUNC, get_system, NULL},
	{ "���� ��砫��� �᫮���       ", CALL_FUNC, get_nach, NULL},
	{ "���� ��ࠬ��஢ ���பᨬ�樨", CALL_FUNC, get_param, NULL},
	{ "������ � �᭮���� ����      ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM graph_menu[] = {
	{ "����騥 p�������      ", CALL_FUNC, draw_graph, NULL},
	{ "�p������� � ���������묨", CALL_FUNC, compare_graph, NULL},
	{ "����������� p�����⮢ ", CALL_FUNC, store_res, NULL},
	{ "������ � �᭮���� ���� ", MAIN_MENU, NULL, NULL},
	END_OF_MENU
};

MENU_ITEM main_menu[] = {
	{ "���⠭���� �����  ", ANOTHER_MENU, NULL, task_menu},
	{ "���� ��⮤�       ", ANOTHER_MENU, NULL, met1},
	{ "��� ��⮤�     ", ANOTHER_MENU, NULL, met2},
	{ "����஥��� ��䨪��", ANOTHER_MENU, NULL, graph_menu},
	{ "����� ࠡ���       ", CALL_FUNC, end_of_work, NULL},
	END_OF_MENU
};

void ntext (void)
{
	textcolor (NORM_COLOR);
	textbackground (NORM_BACKGROUND);
}

void htext (void)
{
	textcolor (HIGH_COLOR);
	textbackground (HIGH_BACKGROUND);
}

void hide_cursor (void)
{
	gotoxy (1, 25);
}

void menu (void)
{
MENU_ITEM * menu_pointer;
int i, n, y, cur_item, ch;
unsigned new_item;
start:
	cur_item = 0;
cont:
	cls();
	menu_pointer = cur_menu;
	ch = 0;
	for (n=0;;n++) {
		if (menu_pointer -> name[0] == 0) break;
		y = strlen (menu_pointer -> name);
		if (ch < y) ch = y;
		menu_pointer += 1;
	}
	y = (24 - n) / 2;
	ntext();
	gotoxy (MENU_X-2, y-2);
	putch ('�');
	for (i=0; i<ch+2; i++) putch ('�');
	putch ('�');
	for (i=0; i<n+2; i++) {
		gotoxy (MENU_X-2, y+i-1);
		putch ('�');
		gotoxy (MENU_X+ch+1, y+i-1);
		putch ('�');
	}
	gotoxy (MENU_X-2, y+n+1);
	putch ('�');
	for (i=0; i<ch+2; i++) putch ('�');
	putch ('�');
	menu_pointer = cur_menu;
	gotoxy (MENU_X, y-1);
	for (i=0; i<n; i++) {
		gotoxy (MENU_X, wherey()+1);
		cputs (menu_pointer -> name);
		menu_pointer += 1;
	}
	gotoxy (MENU_X, cur_item + y);
	htext();
	cputs (cur_menu -> name);
	hide_cursor();
	for (;;) {
		ch = getch();
		switch (ch) {
		  case 0 :	switch (getch()) {
					  case LEFT_KEY :
					  case UP_KEY	:	new_item = cur_item - 1; break;
					  case RIGHT_KEY :
					  case DOWN_KEY :	new_item = cur_item + 1; break;
					  case PGUP_KEY :
					  case HOME_KEY :	new_item = 0; break;
					  case PGDN_KEY :
					  case END_KEY :	new_item = n-1; break;
					  default :	continue;
				}
				if (new_item >= n) continue;
				ntext();
				gotoxy (MENU_X, cur_item + y);
				cputs (cur_menu[cur_item].name);
				cur_item = new_item;
				htext();
				gotoxy (MENU_X, cur_item + y);
				cputs (cur_menu[cur_item].name);
				hide_cursor();
				break;
		  case SPACE :
		  case CR :	switch (cur_menu[cur_item].command) {
						case CALL_FUNC :	cur_menu[cur_item].func();
											goto start;
						case ANOTHER_MENU : cur_menu = cur_menu[cur_item].new;
											goto start;
						case MAIN_MENU :	cur_menu = main_menu;
											goto start;
					}
					break;
		  default :	continue;
		}
	}
}

void main (void)
{
	rang = h = t0 = n = store_n = -1;
	if ((main_point = malloc (65535u)) == NULL) goto no_mem;
	if ((store_point = malloc (65535u)) == NULL) goto no_mem;
	cur_menu = main_menu;
	menu ();
no_mem:
	clrscr();
	cputs ("Not enough memory.");
}
