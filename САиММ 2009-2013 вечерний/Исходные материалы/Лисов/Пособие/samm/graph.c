#include	<graphics.h>
#include	<values.h>
#include	<conio.h>
#include	<stdlib.h>
#include	<string.h>
#include	"methods.h"
#include	"main.h"
#include	"samm.h"

#define		KOORD_COLOR		LIGHTGRAY

static int main_colors[5] = {
	WHITE,
	YELLOW,
	LIGHTCYAN,
	LIGHTBLUE,
	LIGHTGREEN
};

static int store_colors[5] = {
	LIGHTGRAY,
	BROWN,
	CYAN,
	BLUE,
	GREEN
};

float xmin, xmax;
int x_cent, y_cent;

int init_gr (void)
{
int dr = EGA;
int mode = EGAHI;
	initgraph (&dr, &mode, "");
	return 1;
}

void def_bounds (int n, float *point)
{
int i, j;
float tmp;
	xmin = MAXFLOAT;
	xmax = MINFLOAT;
	for (i=0; i<rang; i++)
		for (j=0; j<n; j++) {
			tmp = point[j*rang+i];
			if (tmp < xmin) xmin = tmp;
			if (tmp > xmax) xmax = tmp;
		}
	y_cent = 349 * xmax / (xmax - xmin);
	x_cent = -639 * a / (b - a);
}

void draw_koord (void)
{
	setcolor (KOORD_COLOR);
	line (0, y_cent, 640, y_cent);
	line (x_cent, 0, x_cent, 350);
	gcvt (a, 5, strbuf);
	outtextxy (0, 170, strbuf);
	gcvt (b, 5, strbuf);
	outtextxy (600, 170, strbuf);
	gcvt (xmax, 5, strbuf);
	outtextxy (310, 0, strbuf);
	gcvt (xmin, 5, strbuf);
	outtextxy (310, 340, strbuf);
}

static void put_item (int n)
{
	if (n < rang)	cprintf ("�㭪�� X%1d", n+1);
	else 			cputs   ("Done      ");
}

int req_x[5];
void get_req_num (void)
{
int i, ch;
unsigned new_item, cur_item;
	for (i=0; i<rang; i++) req_x[i] = 1;
	for (i=rang; i<5; i++) req_x[i] = 0;
	ntext();
	clrscr();
	for (i=0; i<=rang; i++) {
		gotoxy (34, i+11);
		cputs ((req_x[i]) ? "�  " : "   ");
		put_item (i);
	}
	cur_item = 0;
	htext();
	gotoxy (37, 11);
	put_item (0);
	hide_cursor();
	for (;;) {
		ch = getch();
		switch (ch) {
		  case 0 :	switch (getch()) {
					  case LEFT_KEY :
					  case UP_KEY	:	new_item = cur_item - 1; break;
					  case RIGHT_KEY :
					  case DOWN_KEY :	new_item = cur_item + 1; break;
					  case PGUP_KEY :
					  case HOME_KEY :	new_item = 0; break;
					  case PGDN_KEY :
					  case END_KEY :	new_item = rang-1; break;
					  default :	continue;
				}
				if (new_item > rang) continue;
				ntext();
				gotoxy (37, cur_item + 11);
				put_item (cur_item);
				cur_item = new_item;
				htext();
				gotoxy (37, cur_item + 11);
				put_item (new_item);
				hide_cursor();
				break;
		  case SPACE :
		  case CR :	if (cur_item == rang) goto end;
					else {
						ntext();
						gotoxy (34, cur_item+11);
						req_x[cur_item] = !req_x[cur_item];
						cputs ((req_x[cur_item]) ? "�  " : "   ");
					}
					break;
		  default :	continue;
		}
	}
end:;
}

static void dr_gr (float *main_point, int *main_colors)
{
int i, j;
float t, x1;
	for (i=0; i<rang; i++) {
		if (!req_x[i]) continue;
		t = a;
		setcolor (main_colors[i]);
		x1 = main_point[i];
		moveto (0, 349 * (xmax - x1) / (xmax - xmin));
		for (j=1; j<n; j++) {
			x1 = main_point [j*rang+i];
			lineto (639 * (t - a) / (b - a),
						349 * (xmax - x1) / (xmax - xmin));
			t += h;
		}
	}
}

void draw_graph (void)
{
int i;
	if (n<0) {
		qtext();
		clrscr();
		gotoxy (30, 12);
		cputs ("���⥬� �� p�襭�");
		getch();
		return;
	}
	get_req_num();
	for (i=0; i<rang; i++) if (req_x[i]) goto cont;
	return;
cont:
	if (!init_gr()) return;
	def_bounds (n, main_point);
	draw_koord();
	dr_gr (main_point, main_colors);
	getch();
	restorecrtmode();
}

void compare_graph (void)
{
int i;
	if (n<0) {
		qtext();
		clrscr();
		gotoxy (30, 12);
		cputs ("���⥬� �� p�襭�");
		getch();
		return;
	}
	if (store_n < 0) {
		qtext();
		clrscr();
		gotoxy (25, 12);
		cputs ("��� ����������� p�����⮢");
		getch();
		return;
	}
	get_req_num();
	for (i=0; i<rang; i++) if (req_x[i]) goto cont;
	return;
cont:
	if (!init_gr()) return;
	def_bounds (n, main_point);
	draw_koord();
	dr_gr (main_point, main_colors);
	getch();
	dr_gr (store_point, store_colors);
	getch();
	restorecrtmode();
}

void store_res (void)
{
	if (n<0) {
		qtext();
		clrscr();
		gotoxy (30, 12);
		cputs ("���⥬� �� p�襭�");
		getch();
		return;
	}
	store_n = n;
	memcpy (store_point, main_point, 65535u);
}
