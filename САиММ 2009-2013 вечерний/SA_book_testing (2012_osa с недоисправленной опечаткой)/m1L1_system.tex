\begingroup
\input{graphcommon}

\chapter{Системы и~их свойства}
% \chapter{Теория систем}

\begingroup%\renewcommand{\baselinestretch}{1.2}\normalfont
% \epigraph{Истина бывает так ужасна, потому что представляет собой малую часть конструкции Мироздания, доступную пониманию исследователя.}
% {Пак Хин}
\section{Предпосылки возникновения теории систем}

\epigraph{Теория Всеобщих Явлений представляет из себя теорию, систематизирующую и~объясняющую все факты жизни на основе их внутренней взаимоцелесообразности.}
{Мифология Аквариума}


В~настоящее время знания человека о~природе разрослись до такой степени, 
что не представляется возможным охватить не только весь их объём, 
но даже и~отдельные его области, такие как математика, физика, биология и~т.\,п. 

До XX в. в~качестве  междисциплинарного формализованного  языка использовалась математика, 
но её изобразительных средств оказалось недостаточно для эффективного  переноса знаний между современными научными  дисциплинами.

Учёные всё сильнее углубляются в~изучение специальных областей, 
часто не отдавая себе отчёта о~полезности этих знаний. 
С~другой стороны, для современного учёного необходимо получение сведений из других отраслей науки. 
Появление таких дисциплин, как биофизика, физическая химия, биохимия, бионика, математическая лингвистика, 
требует сочетания сведений из различных областей. 
Таким образом, налицо реальное противоречие в~развитии науки. 

Эти причины явились предпосылками возникновения \termin{общей теории систем,} которая оформилась как самостоятельная дисциплина 
в~сороковых-пятидесятых годах ХХ века и~призвана помочь человечеству в~преодолении недостатков узкой специализации, 
усилении междисциплинарных связей, развитии диалектического видения мира, системного мышления.

\termin{Системный анализ} со временем стал меж- и~наддисциплинарным курсом, 
обобщающим методологию исследования сложных технических и~социальных систем.

\endgroup
\section{Место теории систем в~системе наук}
\epigraph{\begin{stanza}
<<Вот где водится Снарк!>> "--- возгласил Балабон\\
Указав на вершину горы;\\
И~матросов на берег вытаскивал он,\\
Их подтягивал за вихры.\end{stanza}}
{\Snark}

\termin{Общая теория систем% (ОТС)
} "--- научная дисциплина, изучающая самые фундаментальные понятия и~аспекты систем.

Она изучает различные явления, отвлекаясь от их конкретной природы и~основываясь лишь на формальных взаимосвязях между различными составляющими их факторами и~на характере их изменения под влиянием внешних условий,
при этом результаты всех наблюдений объясняются лишь взаимодействием их компонентов (например, характером их организации и~функционирования), а~не с~помощью непосредственного обращения к~природе вовлечённых в~явления механизмов (будь они физическими, биологическими, экологическими, социологическими или концептуальными)~\cite{bertalanfi}.

Для %ОТС 
общей теории систем
объектом исследования является не <<физическая реальность>>, а~<<система>>, т.\,е. абстрактная формальная взаимосвязь между основными признаками и~свойствами.

При системном подходе объект исследования представляется как система. Само понятие <<система>> может быть относимо к~одному из методологических понятий, поскольку рассмотрение объекта исследования как системы, либо отказ от такого рассмотрения зависит от специфики конкретной задачи и~от самого исследователя.


Современные представления единого, общего, универсального научного знания выражены в~законах и~моделях систем, методах анализа и~синтеза систем. Овладение единым научным знанием обеспечивает формирование у~студентов фундаментальных основ научного знания, которые способствуют повышению качества обучения при сокращении сроков, улучшению понимания, развитию таких познавательных функций как описание, объяснение и~предсказание.


\subsection{Предмет теории систем}

Предмет  теории систем "--- \termin{системы} произвольной природы.

Общепринятого определения системы нет и~(по всей видимости) не может существовать,
однако есть множество частных определений, применяемых в~той или иной области.


\subsection{Понятие системы}
\epigraph{\begin{stanza}Так внемлите, друзья! Вам поведаю я\\Пять бесспорных и~точных примет,\\
По которым поймёте "--- если только найдёте, "---\\Кто попался вам "--- Снарк или нет.\end{stanza}}
{\Snark}

Существует много определений системы.
\begin{enumerate}
\item \termin{Система} есть комплекс элементов, находящихся во взаимодействии.
\item \termin{Система} "--- это множество объектов вместе с~отношениями этих объектов.
\item \termin{Система} "--- множество элементов, находящихся в~отношениях или связях друг с~другом, образующих целостность или органическое единство (толковый словарь).

Термины <<отношение>> и~<<взаимодействие>> используются в~самом широком смысле, включая весь набор родственных понятий, таких как ограничение, структура, организационная связь, соединение, зависимость и~т.\,д.
Таким образом, система $S$ представляет собой упорядоченную пару $S=(A, R)$, где $A$ "--- множество элементов; $R$ "--- множество отношений между $A$.

\item \termin{Система} "--- это полный, целостный набор элементов (компонентов), взаимосвязанных и~взаимодействующих между собой так, чтобы могла реализоваться функция системы.
\end{enumerate}

Мы чаще всего будем использовать определение \termin{системы} $S$ как совокупности входов $X$, выходов $Y$
и, возможно, внутренней структуры $Z$.
Такому представлению о~системе соответствует \termin{феноменологическая модель} (рис.~\ref{fig:sys_phenomen}).
\begin{figure}[h]
\center
% \resizebox{\approvedImageWidth}{!}{
\begin{tikzpicture}[line cap=round,line join=round,x=3ex,y=3ex]
\foreach \y in {-1,0,1}
{
	\draw[axisline] (-6,\y) -- (-3,\y);
	\draw[axisline] (3,\y) -- (6,\y);
}
\draw (-3,-1.5) rectangle +(6,3);
% \draw[->,very thick] (0,2.5) -- (0,1.5);
\draw (-6,1.5) node {Входы};
\draw (6,1.5) node {Выходы};
% \draw (0,3) node {$Z$};
\draw (0,0) node {$Z$};
\draw (-6,-1.5) node {$X$};
\draw (6,-1.5) node {$Y$};
\end{tikzpicture}
% }
\caption{\label{fig:sys_phenomen}Феноменологическая модель системы}
\end{figure}

Если нас интересует сам факт зависимости, но не её вид, можно воспользоваться 
\termin{топологическим} представлением системы как декартова произведения \termin{входов} и~\termin{выходов:}
\begin{equation}\label{eq:sys_phenomen_xy}
S \subset X \times Y
\end{equation}
или, если ввести в~рассмотрение ещё и~\termin{внутренние параметры:}
\begin{equation}\label{eq:sys_phenomen_xzy}
S \subset X \times Z \times Y
\end{equation}

Если есть функциональная связь между входами и~выходами \mbox{$S: X \to Y,$}
характер этой связи называется \termin{реакцией} системы $R$:
\begin{equation}\label{eq:sys_react}
R: {X \times Z}
 \to Y
\end{equation}

% \begin{equation}\label{eq:sys_react}
% R: {X \times Z}^{(F)}
%  \to Y
% \end{equation}

Помимо реакции вводятся:
\begin{itemize}
\item \termin{вспомогательные функции} "--- функции, которые накладывают некоторые ограничения
на взаимодействие внутренних параметров, входов и~выходов, например
$$\sum \text{входных~воздействий} = 1;$$
\item производящие функции "--- текущее значение выхода определяется:
\begin{itemize}
\item текущим временем;
\item начальным значением входа;
\item предыдущим значением входа;
\item текущим значением входа;
\item предыдущим значением выхода.
\end{itemize}
Характер этой зависимости определяется \termin{производящей функцией:}
$$Y_k = f(X_0, X_{k-1}, X_k, Y_{k-1}, t_k)$$
\end{itemize}

Исследование объекта как системы предполагает использование ряда систем представлений (категорий), среди которых основными являются:
\begin{descerate}
\item[Структурное] представление связано с~выделением элементов системы и~связей между ними.
\item [Функциональное] представление систем "--- выделение совокупности функций (целенаправленных действий) системы и~её компонентов, направленное на достижение определённой цели.
\item [Макроскопическое] представление "--- понимание системы как нерасчленимого целого, взаимодействующего с~внешней средой.
\item [Микроскопическое] представление основано на рассмотрении системы как совокупности взаимосвязанных элементов. Оно предполагает раскрытие структуры системы.
\item [Иерархическое] представление основано на понятии \termin{подсистемы}.

Подсистема получается при разложении (декомпозиции) системы на части, обладающие системными свойствами.
Подсистему следует отличать от элемента системы "--- неделимого на более мелкие части (с~точки зрения решаемой задачи). Система может быть представлена в~виде совокупности подсистем различных уровней, составляющих \termin{системную иерархию,} которая замыкается снизу только элементами.
\item [Процессуальное] представление предполагает понимание системного объекта как динамического объекта, характеризующегося последовательностью его состояний во времени.
\end{descerate}





Рассмотрим определения других понятий, тесно связанных с~системой и~её характеристиками.

\subsection{Объект}
\termin{Объектом} познания является часть реального мира, которая выделяется и~воспринимается как единое целое в~течение длительного времени. Объект может быть материальным и~абстрактным, естественным и~искусственным.

Реально объект обладает бесконечным набором свойств различной природы. Практически в~процессе познания взаимодействие осуществляется с~ограниченным множеством свойств, лежащих в~пределах возможности их восприятия и~необходимости для цели познания. Поэтому система как образ объекта задаётся на конечном множестве отобранных для наблюдения свойств.

\subsection{Внешняя среда}
Понятие <<система>> возникает там и~тогда, где и~когда мы материально или умозрительно проводим замкнутую границу
внутри неограниченного или некоторого ограниченного множества элементов.
%  между неограниченным или некоторым ограниченным множеством элементов. 
Те элементы с~их соответствующей взаимной обусловленностью, которые попадают внутрь, "--- образуют систему.

Те элементы, которые остались за пределами границы, образуют множество, называемое в~теории систем \termin{<<системным окружением>>} или просто \termin{<<окружением>>,} или \termin{<<внешней средой>>}.

Из этих рассуждений вытекает, что немыслимо рассматривать систему без её внешней среды.
Система формирует и~проявляет свои свойства в~процессе взаимодействия с~окружением, являясь при этом (с~точки зрения пользователя) ведущим компонентом этого воздействия.
В~зависимости от воздействия на окружение и~характера взаимодействия с~другими системами \termin{функции систем} можно расположить по возрастающему рангу следующим образом:
\begin{itemize}
\item пассивное существование;
\item материал для других систем;
\item обслуживание систем более высокого порядка;
\item противостояние другим системам (выживание);
\item поглощение других систем (экспансия);
\item преобразование других систем и~сред (активная роль).
\end{itemize}

Всякая система может рассматриваться, с~одной стороны, как \termin{подсистема} системы более высокого порядка \termin{(надсистемы, сверхсистемы, суперсистемы),} а~с~другой, как надсистема системы более низкого порядка \termin{(подсистемы).} Например, система <<производственный цех>> входит как подсистема в~систему более высокого ранга "--- <<фирма>>. В~свою очередь, надсистема <<фирма>> может являться подсистемой <<корпорации>>.

Обычно в~качестве подсистем фигурируют более или менее самостоятельные части систем, выделяемые по определённым признакам, обладающие относительной самостоятельностью, определённой степенью свободы.

\termin{Компонент} "--- любая часть системы, вступающая в~определённые отношения с~другими частями (подсистемами, элементами).

\termin{Элементом} системы является часть системы с~однозначно определёнными свойствами, выполняющая определённые функции и~не подлежащая дальнейшему разбиению в~рамках решаемой задачи (с~точки зрения исследователя).

Понятия \termin{элемент, подсистема, система} взаимопреобразуемы, система может рассматриваться как элемент системы более высокого порядка (\termin{метасистемы}), а~элемент при углубленном анализе "--- как система. То обстоятельство, что любая подсистема является одновременно и~относительно самостоятельной системой, приводит к~двум аспектам изучения систем: на макро- и~микро- уровнях.

При изучении на \termin{макроуровне} основное внимание уделяется взаимодействию системы с~внешней средой. Причём системы более высокого уровня можно рассматривать как часть внешней среды. При таком подходе главными факторами являются целевая функция системы (цель), условия её функционирования. При этом элементы системы изучаются с~точки зрения организации их в~единое целое, влияния на функции системы в~целом.

На \termin{микроуровне} основными становятся внутренние характеристики системы, характер взаимодействия элементов между собой, их свойства и~условия функционирования.

Для изучения системы сочетаются оба компонента. Важно рассмотрение системы как на макро-, так и~на микроуровне.



\section{Структура системы}
\epigraph{\begin{stanza}[0mm]Действительность "--- не бред собачий.\\
Она сложнее и~богаче.\end{stanza}}
{Валентин Берестов}

Под \termin{структурой} системы понимается устойчивое множество отношений, которое сохраняется длительное время неизменным, по крайней мере в~течение интервала наблюдения.
Структура системы отражает определённый уровень сложности по составу отношений на множестве элементов системы или, что эквивалентно, уровень разнообразий проявлений объекта.

\termin{Связи} "--- это отношения, осуществляющие непосредственное взаимодействие между элементами (или подсистемами) системы, а~также с~элементами и~подсистемами окружения.

Связь "--- одно из фундаментальных понятий в~системном подходе. Система как единое целое существует именно благодаря наличию связей между её элементами, т.\,е., иными словами, связи выражают законы функционирования системы. Связи различают по характеру взаимосвязи как прямые и~обратные, а~по виду проявления (описания) "--- как детерминированные и~вероятностные.

\termin{Прямые связи} предназначены для заданной функциональной передачи вещества, энергии, информации или их комбинаций "--- от одного элемента к~другому в~направлении основного процесса.



\termin{Обратные связи}, в~основном, выполняют осведомляющие функции, отражая изменение состояния системы в~результате управляющего воздействия на неё. Открытие принципа обратной связи явилось выдающимся событием в~развитии техники и~имело исключительно важные последствия. Процессы управления, адаптации, саморегулирования, самоорганизации, развития невозможны без использования обратных связей.
% \begin{figure}[h]
% \center
% \begin{tikzpicture}[auto, node distance=2cm,>=latex']
%     % We start by placing the blocks
%     \node [input, name=input] {};
%     \node [block, right of=input] (Obj) {\mbox{Объект\\управления}};
%     \node [block, above of=Obj] (controller) {{Орган\\управления}};
%     \node [output, right of=Obj] (sv) {};
%     \node [output, right of=sv] (output) {};
% 
%     % Once the nodes are placed, connecting them is easy. 
%     \draw [draw,->] (input) -- node {Вход} (Obj);
%     \draw [->] (Obj) -- node [name=y] {Выход} (output);
%     \draw [->] (y) |- node {обратная связь} (controller);
%     \draw [->] (controller) -| node[pos=0.99] {$-$} 
%         node [near end] {$y_m$} (Obj);
% \end{tikzpicture}
% \caption{\label{fig:2}Пример обратной связи}
% \end{figure}

С~помощью обратной связи сигнал (информация) с~выхода системы (объекта управления) передается в~орган управления. Здесь этот сигнал, содержащий информацию о~работе, выполненной объектом управления, сравнивается с~сигналом, задающим содержание и~объём работы (например, с~планом). В~случае возникновения рассогласования между фактическим и~плановым состоянием работы принимаются меры по его устранению.

Основными \termin{функциями} обратной связи являются:
\begin{enumerate}
\item Противодействие тому, что делает сама система, когда она выходит за установленные пределы (например, реагирование на снижение качества).
\item Компенсация возмущений и~поддержание состояния устойчивого равновесия системы (например, реагирование на неполадки в~работе оборудования).
\item Отслеживание внешних и~внутренних возмущений, стремящихся вывести систему из состояния устойчивого равновесия, сведение этих возмущений к~отклонениям одной или нескольких управляемых величин (например, выработка управляющих команд на одновременное появление нового конкурента и~снижение качества выпускаемой продукции).
\item Выработка управляющих воздействий на объект управления по плохо формализуемому закону. Например, установление более высокой цены на энергоносители вызывает в~деятельности различных организаций сложные изменения, которые меняют конечные результаты их функционирования, требуют внесения изменений в~производственно-хозяйственный процесс путем воздействий, которые невозможно описать с~помощью аналитических выражений.
\end{enumerate}

Нарушение обратных связей в~системах разной природы по различным причинам ведёт к~тяжёлым последствиям. Отдельные локальные системы утрачивают способность к~эволюции и~тонкому восприятию намечающихся новых тенденций, перспективному развитию и~научно обоснованному прогнозированию своей деятельности на длительный период времени, эффективному приспособлению к~постоянно меняющимся условиям внешней среды.
Особенностью социально-экономических систем является то обстоятельство, что не всегда удается чётко выразить обратные связи, которые в~них, как правило, длинные, проходят через целый ряд промежуточных звеньев, и~чёткий их просмотр затруднен. Сами управляемые величины нередко не поддаются ясному определению, и~трудно установить множество ограничений, накладываемых на параметры управляемых величин. Не всегда известны также действительные причины выхода управляемых переменных за установленные пределы.

По виду проявления связи можно разделить на детерминированные и~вероятностные.

\termin{Детерминированная (жёсткая)} связь, как правило, однозначно определяет причину и~следствие, даёт четко обусловленную формулу взаимодействия элементов.

\termin{Вероятностная (гибкая)} связь определяет неявную, косвенную зависимость между элементами системы. Теория вероятности предлагает математический аппарат для исследования этих связей, называемый «корреляционными зависимостями».

Для управления системой важны критерии оценки её функционирования и~эффективности её работы.

\termin{Критерии} "--- признаки, по которым производится оценка соответствия функционирования системы желаемому результату (цели) при заданных ограничениях.

\termin{Эффективность} системы "--- соотношение между заданным (целевым) показателем результата функционирования системы и~фактически реализованным.

Функционирование любой произвольно выбранной системы состоит в~переработке входных (известных) параметров и~известных параметров воздействия окружающей среды в~значения выходных (неизвестных) параметров с~учётом факторов обратной связи.


\termin{Вход} "--- всё, что изменяется при протекании процесса (функционирования) системы.

\termin{Выход} "--- результат конечного состояния процесса.

\termin{Процессор} "--- перевод входа в~выход.

Система осуществляет свою связь со средой следующим образом.
Вход данной системы является в~то же время выходом предшествующей, а~выход данной системы "--- входом последующей. Таким образом, вход и~выход располагаются на границе системы и~выполняют одновременно функции входа и~выхода предшествующих и~последующих систем.

\termin{Управление} системой связано с~понятиями прямой и~обратной связи, ограничениями.

Обратная связь как инструмент управления системой предназначена для последовательного выполнения следующих операций:
\begin{itemize}
\item сравнение данных на входе с~результатами на выходе с~выявлением их качественно-количественного противоречия, различия;
\item оценка содержания и~смысла различия;
\item выработка решения, вытекающего из различия;
\item воздействие на вход.
\end{itemize}
\begingroup%\renewcommand{\baselinestretch}{1.1}\normalfont
\termin{Ограничение} обеспечивает соответствие между выходом системы и~требованием к~нему, как к~входу в~последующую систему-потребитель. Если заданное требование не выполняется, ограничение не пропускает его через себя.
Ограничение, таким образом, играет роль согласования функционирования данной системы с~целями (потребностями) потребителя.

Определение функционирования системы связано с~понятием <<проблемной ситуации>>, которая возникает, если имеется различие между необходимым (желаемым) выходом и~существующим (реальным) входом.

\termin{Проблема} "--- это разница между существующей и~желаемой системами. Если этой разницы нет, то нет и~проблемы.
Решить проблему "--- значит скорректировать старую систему или сконструировать новую, желаемую.

\termin{Состоянием системы} называется совокупность существенных свойств, которыми система обладает в~каждый момент времени.




\section{Классы систем}
\epigraph{\begin{stanza}...А~далее сделаем так:\\Разобьем их на несколько кучек\\
И~рассмотрим отдельно "--- Лохматых Кусак\\И~отдельно "--- Усатых Колючек.\end{stanza}}
{\Snark}

Любая классификация условна и~является всего лишь моделью реальности.

Так, по различным критериям системы можно разделить на следующие классы:
% Системы разделяются на:
\begin{itemize}
\item физические и~абстрактные;
\item динамические и~статические;
\item естественные и~искусственные;
\item системы с~управлением и~без управления;
\item непрерывные и~дискретные (аналоговые и~цифровые);
\item детерминированные и~стохастические;
\item открытые и~замкнутые;
\item большие и~небольшие;
\item простые и~сложные.
\end{itemize}

Выделяют три свойства системы, по которым можно разделить системы на простые и~сложные~\cite{Tarasenko_PrikladnoySA}:
\begin{itemize}
\item робастность (живучесть, запас прочности);
\item эмерджентность (системный эффект);
\item однородность связей между элементами.
\end{itemize}

\termin{Робастность (живучесть, запас прочности)} "--- свойство системы сохранять частичную работоспособность при отказе отдельных элементов или подсистем, что обеспечивается функциональной избыточностью сложной системы. Простая система может находиться лишь в~одном из двух состояний "--- работоспособности или полного отказа.

Меры обеспечения робастности "--- обязательное троирование информации, горячее и~холодное резервирование.

\termin{Эмерджентность (системный эффект)} "--- наличие у~системы свойств,
которыми не обладает каждый элемент в~отдельности 
(а~каждый её элемент может обладать свойствами, которых нет у~системы).

\termin{Сложной} обычно называют систему, состоящую из элементов разных типов и~обладающую \termin{разнородными связями} между ними.

\termin{Большой системой} называют систему, включающую значительное число однотипных элементов и~однотипных связей.




% Чаще всего искусственные системы "--- системы с~управлением.
% Естественными системами тоже можно управлять, но эти воздействия ограничены.


% Если модель системы многоуровневая, иерархическая, содержит подклассы, она может быть продолжена и~развита без изменения ее верхних уровней. Например, классификация по происхождению:
% 
% Классификация систем по описанию переменных:
% 
% Оператором S системы называется связь между входными и~выходными переменными. Соответственно, по особенностям операторов системы классифицируются следующим образом.
% 
% \termin{Большие системы} "--- системы, моделирование которых затруднительно вследствие их размерности. Существуют два способа перевода их в~малые:\\
% 1) разработка более мощных ЭВМ;\\
% 2) декомпозиция многомерной задачи на совокупность связанных задач меньшей размерности.
% 
% \termin{Сложные системы} "--- системы, в~моделях которых не хватает информации для эффективного управления.
% Действительно, признак простоты системы "--- это достаточность информации для управления. Если же полученное с~помощью модели управления приводит к~неожиданным, непредвиденным или нежелательным результатам, т.\,е. отличающимися от предсказанных моделью, это может быть объяснено недостатком информации и~интерпретироваться как сложность системы.
% 
% Таким образом, свойство простоты или сложности управляемой системы является свернутым отношением между нею и~управляющей системой, точнее, между системой и~ее моделью. Это отношение объективно (примеры: кодовый замок, родной язык, умение обращаться с~компьютером, водить автомобиль и~т.п.).
% 
% Считается также, что сложную систему можно охарактеризовать тремя основными принципами:
% 1) свойством робастности (сохранения частичной работоспособности при отказе отдельных элементов или подсистем, что объясняется функциональной избыточностью сложной системы (простая система может находиться лишь в~одном из двух состояний (работоспособности или полного отказа);

\endgroup


\section{Формальное представление системы}
\epigraph{Начало олимпиадной задачи по информатике:
<<В~кубическом мире в~квадратной луже лежит свинья, откормленная до~размеров прямоугольного параллелепипеда...>>}
{\MathFolk}

Основой формального представления системы является теория множеств
и~основанная на ней теория графов.

Построить формальное представление системы значит
построить небольшую теорию, описывающую систему
(<<системное представление системы>>).

Существует два способа построения \termin{формальной модели} системы (теории, описывающей систему):
\begin{descerate}
\item[Аксиоматический подход] предполагает, что вначале формулируются основные положения (аксиомы),
а~затем на их основе посредством рассуждений доказываются другие утверждения (теоремы).

Аксиомы одной теории в~другой могут оказаться теоремами.

\item[Декларативный подход] предполагает, что составляется совокупность определений,
которая раскрывает все свойства системы.
\end{descerate}

Основная проблема аксиоматического подхода
"--- выбор необходимого и~достаточного набора аксиом.

Существует четыре основных положения построения аксиоматической теории:
\begin{descerate}
\item[Полнота] "--- в~рамках системы аксиом можно ответить на все вопросы и~доказать или опровергнуть все утверждения.

\item[Непротиворечивость] "--- нельзя одновременно доказать утверждение и~его отрицание.

\item[Разрешимость] "--- %набор аксиом может обосновать любое свойство.
наличие алгоритма, определяющего, выводимо ли утверждение из аксиом.

\item[Вычислимость] "--- любое положение можно разложить до аксиом.

\end{descerate}

\pagebreak[3]

В~теории алгебраических систем существует теорема Гёделя:\nopagebreak
\begin{importantly}
Всякая полная система противоречива.
\end{importantly}

Практически используется минимально необходимый набор аксиом (неполный и~непротиворечивый).




\section{Свойства и~требования к~математическим моделям}
\epigraph{Собрались как-то физики с~математиками и~создали супер-пупер точную модель для предсказания погоды. Только вот незадача "--- для ввода всех потребных параметров и~показаний датчиков требуется время, стремящееся к~бесконечности.}
{\MathFolk}
\termin{Модель} "--- мысленный или знаковый образ моделируемого объекта (оригинала)~\cite{Samar_MatMod}.

Модель характеризуется следующими свойствами и~параметрами:
\begin{descerate}
\item[Универсальность] математической модели, как правило, связана с~классификацией объектов моделирования.

\termin{Универсальная} модель позволяет моделировать широкий класс объектов,
\termin{специальная} "--- более узкий класс (или даже только один конкретный объект).
Время расчёта универсальной модели, как правило, больше времени расчёта специальной.

Универсальность модели должна соответствовать решаемой задаче
с~учётом точности решения и~частоты повторения расчётов.

\item[Алгоритмическая надёжность] "--- %надёжность программы.
вероятность получения правильных результатов при соблюдении оговорённых ограничений на применение метода.

Элементы ненадёжности:
\begin{itemize}
\item на этапе создания модели недостаточно полно определена область допустимых значений модели
(в~большинстве  случаев модель срабатывает, но в~данном конкретном случае происходит сбой);
\item при создании модели сложной системы нельзя описать её точно;
в~модель вносится элемент \emph{эвристики} "--- математически необоснованные соотношения и~решения.
\end{itemize}


\item[Обусловленность]\strut\par\nopagebreak
Модель считается \termin{хорошо обусловленной}, если малому приращению аргумента соответствует
малое приращение  функции.

Плохая обусловленность способствует алгоритмической ненадёжности модели.


% Модель характеризуется следующими параметрами:


\item[Точность] должна соответствовать цели решения задачи.

\item[Объём занимаемой памяти.]
\item[Длительность одновариантного анализа.]
\end{descerate}
Объём занимаемой памяти и~длительность одновариантного анализа составляют \termin{цену модели}.






\section{Классификация математических моделей}
\epigraph{Животные делятся на:\\
а) принадлежащих Императору,\\
б) набальзамированных,\\
в) прирученных,\\
г) молочных поросят,\\
д) сирен,\\
е) сказочных,\\
ж) бродячих собак,\\
з) включённых в эту классификацию,\\
и) бегающих как сумасшедшие,\\
к) бесчисленных,\\
л) нарисованных тончайшей кистью из верблюжьей шерсти,\\
м) прочих,\\
н) разбивших цветочную вазу,\\
о) похожих издали на мух.
}
{Хорхе Луис Борхес}
В~каждой предметной области может быть своя классификация (возможно, связанная с~классификацией объектов).

Четыре критерия разбиения представлены в~таблице~\ref{tab:mm_class}.

\ifepigraph{\linespread{1.1}}

\begin{table}[ht]
\caption{Основные критерии классификации математических моделей}
\small
\label{tab:mm_class}
\bfseries
\begin{tabularx}{\linewidth}%{|@{\hspace{4ex}}X@{\ $\longleftrightarrow$\ \ }X@{\hspace{4ex}}|}
{|@{}X@{}r@{\;\;$\longleftrightarrow$\;\;}l@{}X@{}|}
% \hline
% \textbf{Феноменологические} (причинно-следственные, управленческие) модели&
% \textbf{Поведенческие} модели (модели достижения цели)\\\hline
% \textbf{Временн\'{ы}е} (динамические) модели&
% \textbf{Алгебраические} (статические) модели\\\hline
% \textbf{Детерминированные}  модели&
% \textbf{Стохастические} (вероятностные) модели\\\hline
% \textbf{Аналитические}  модели&
% \textbf{Имитационные} модели\\\hline

\hline&
Феноменологические & 
Поведенческие
&\\\hline&
Временн\'{ы}е (динамические) &
Алгебраические (статические)
&\\\hline&
Детерминированные&
Стохастические (вероятностные)
&\\\hline&
Аналитические  &
Имитационные 
&\\\hline
\end{tabularx}
\end{table}

\emph{Феноменологические (причинно-следственные, управленческие) модели} в~большинстве случаев представляются как декартово произведение входов и~выходов.\sloppy
\begin{figure}[!h]
	\begin{tikzpicture}[node distance = 8ex, auto, -latex']
		\coordinate (in) {};
		\node[rectangle, draw, text width=2cm, text centered, right=of in] (modell) {$Z_\text{вн}$};
		\coordinate[right=of modell] (out) {};
		\coordinate[below=  0.8 of modell] (control) {};
		\draw (in) -- node {вх} (modell);
		\draw (modell) -- node {вых} (out);
		\draw (control) -- node[swap] {управляющее воздействие} (modell);
	\end{tikzpicture}
\caption{Феноменологическая модель}
\label{fig:top}
\end{figure}

\emph{Поведенческие модели (модели достижения цели)} не имеют входа и~выхода.
Задача "--- посмотреть, как эта модель будет вести себя во времени.

Объектов, которые описываются чисто феноменологической или чисто поведенческой моделью,
очень мало.


\emph{Временн\'{ы}е (динамические) модели}
создаются для исследования объектов в~зависимости от времени.
Как правило, динамические модели представляются в~форме системы линейных или нелинейных дифференциальных уравнений.

В~\emph{алгебраических (статических)} моделях и~системах время тоже может фигурировать,
но как дискретный интервальный параметр.
Как правило, статические модели представляются в~форме системы алгебраических уравнений.

Критерием принадлежности к~тому или иному типу является наличие времени как непрерывного параметра модели.


\emph{Стохастические (вероятностные) модели}
используют аппарат теории вероятностей.

\emph{Детерминированные модели}
рассматривают конкретные решения.
% (это не обязательно одно значение
% "--- возможно изменение в~пределах диапазона (допуск) и~даже, возможно нас интересуют распределение по диапазону)
Моделирование не использует аппарат теории вероятностей.


\emph{Аналитические модели} построены на основе математических выражений.
При построении таких моделей исследователь отказывается от детального описания системы, оставляя лишь наиболее существенные, с~его точки зрения, компоненты и~связи между ними, и~использует достаточно малое число правдоподобных гипотез о~характере взаимодействия компонентов и~структуры системы.

\emph{Имитационные модели} появились вместе с~компьютерами.
Это программное моделирование, имитирующее работу объекта.
Целью построения имитационной модели является максимальное приближение модели к~конкретному  объекту и~достижение максимальной точности его описания.
% 
Для построения имитационных моделей используются, например, E-сети, сети Петри и~языки GPSS, ЛИСП.



% \section{Элементы общей теории систем (ОТС)}
\section{Иерархические и~неиерархические структуры}
\epigraph{Это нормальные люди деревья сажают, а~программисты их сначала строят, а~потом обходят.}
{\MathFolk}

Иерархия "--- это расположение частей целого в~порядке от высшего к~низшему. Термин «иерархия» (многоступенчатость) определяет упорядоченность компонентов системы по степени важности.

Между уровнями иерархии структуры могут существовать взаимоотношения строгого подчинения компонента нижележащего уровня одному из компонентов вышележащего уровня, то есть отношения древовидного порядка. Такие иерархии называют сильными или иерархиями типа «дерево».
% 
Однако между уровнями иерархической структуры необязательно должны существовать отношения древовидного характера. Могут иметь место связи и~в~пределах одного уровня иерархии. Нижележащий компонент может подчиняться нескольким компонентам вышележащего уровня "--- это иерархические структуры со слабыми связями.

Для иерархических структур характерно наличие управляющих и~исполнительных компонентов. Могут существовать компоненты, являющиеся одновременно и~управляющими и~исполнительными.

Различают строго и~нестрого иерархические структуры.

В~системах \termin{строго иерархической структуры} присутствует три следующих признака:

\begin{itemize}%[nolistsep]
\item в~системе имеется один главный управляющий компонент, который имеет не менее двух связей;
\item имеются исполнительные компоненты, каждый из которых имеет только одну связь с~компонентом вышележащего уровня;
\item связь существует только между компонентами, принадлежащими двум соседним уровням, при этом компоненты низшего уровня связаны только с~одним компонентом высшего уровня, а~каждый компонент высшего уровня не менее, чем с~двумя компонентами низшего.
\end{itemize}

\begin{figure}[!ht]
\begin{tikzpicture}[graph,x=2ex,y=4ex]
	\foreach \y in {0,2,4}
	{
		\draw[dashed] (-6,\y) -- (6,\y);
	}
	
	\node[circlevertex,fill=white] at (0,4)	(h1)  	{$1$};
	\foreach \x in {-4,0,4}
	{
		\node[circlevertex,fill=white] at (\x,2)	(h2\x)  	{$2$};
		\draw (h1) -- (h2\x);
	}
	\foreach \x/\u in {-5/-4,-3/-4,-1/0,1/0,3/4,5/4}
	{
		\node[circlevertex,fill=white] at (\x,0)	(h3\x)  	{$3$};
		\draw (h2\u) -- (h3\x);
	}
	
\end{tikzpicture}
\caption{Граф строго иерархической структуры}
\label{fig:rsr_hier}
\end{figure}



На рис.~\ref{fig:rsr_hier} приведён пример графа строго иерархической структуры.

Если хотя бы один признак отсутствует, структура графа не является строго иерархической.

\begin{figure}[!ht]
\begin{tikzpicture}[graph,x=2ex,y=4ex]
	\foreach \y in {0,2,4}
	{
		\draw[dashed] (-1,\y) -- (13,\y);
	}
	
	\node[circlevertex,fill=white] at (6,4)	(h1)  	{$1$};
	\foreach \x in {1,6}
	{
		\node[circlevertex,fill=white] at (\x,2)	(h2\x)  	{$2$};
		\draw (h1) -- (h2\x);
	}
	\foreach \x/\u in {0/21,2/21,4/26,6/26,8/26,12/1}
	{
		\node[circlevertex,fill=white] at (\x,0)	(h3\x)  	{$3$};
		\draw (h\u) -- (h3\x);
	}
	
\end{tikzpicture}
\caption{Граф нестрогой иерархической структуры}
\label{fig:rsr_nhier}
\end{figure}

На рис.~\ref{fig:rsr_nhier} приведён пример графа нестрогой иерархической структуры.
% 
В~нём отсутствует третий признак "--- элемент третьего уровня связан напрямую с~элементом первого уровня.


Структуры графов как на рис.~\ref{fig:rsr_hier}, так и~на рис.~\ref{fig:rsr_nhier}
"--- трёхуровневые.




% \section{}
% \section{}
% \section{}



\endgroup

