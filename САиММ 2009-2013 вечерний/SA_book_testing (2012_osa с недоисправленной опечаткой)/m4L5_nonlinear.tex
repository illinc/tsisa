\begingroup
% \linespread{1.2}

\chapter{Нелинейные математические модели}
\epigraph{Козебар Мат характеризуется совершенной неприменимостью любых логических концепций к~паттернам происходящего в~этот период. То есть "--- происходящее ясно, но не поддаётся линейному описанию.}
{Мифология Аквариума}
Предположение о~линейности модели часто является слишком большим упрощением действительности.
Линейность сильно огрубляет модель, делая её неадекватной.
% 
Любая модель или элемент, в~которых не соблюдается принцип суперпозиции, являются нелинейными.

Большинство природных и~технологических явлений и~объектов,
изучаемых разными отраслями науки и~техники;
процессов, происходящих в~обществе, производственных процессов
имеют нелинейный характер.


Существуют принципиально нелинейные объекты (в~том числе явления), для которых применение линейных моделей приводят к~грубым искажениям картины. Это прежде всего системы, для которых изменение масштаба воздействия приводит к~качественному изменению результата.

Типичным примером могут служить механические системы с~сухим трением, для которых малая сила не порождает движения, а~большая "--- порождает; вообще, наличие любых барьеров подобного рода "--- это типичный нелинейный эффект.

Существенно нелинейной является также задача об изучении околокритического состояния объекта, зависящего от параметров, когда при их изменении устойчивость сменяется неустойчивостью или один тип движения "--- другим и~т.\,п. Во всех таких случаях надо применять методы нелинейного анализа.

Математическое моделирование нелинейных систем позволяет предусмотреть и~предотвратить
негативные ситуации в~функционировании реальных процессов и~систем.

Кроме того, математическое моделирование позволяет рассмотреть процессы, длящиеся годами (и~даже веками)
за малое время.
При этом материальные затраты на эксперимент существенно ниже затрат на создание физической модели.

Кроме того, исследовать процессы вблизи критических точек
невозможно при физическом моделировании,
не говоря уже о~возможности многократного проведения эксперимента.

Поэтому важность создания и~исследования нелинейной математической модели очевидна.

\section{Понятие нелинейной математической модели}
\epigraph{Прямая "--- частный случай кривой.}
{\MathFolk}
Нелинейный элемент "--- элемент, у~которого зависимость $y(x)$ выхода от входа "--- нелинейная функция, то есть для неё не выполняется \termin{принцип суперпозиции} (рис.~\ref{fig:nel_elem}).


\begin{figure}[!h]
	\begin{tikzpicture}[node distance = 12ex,label distance=3ex, auto, -latex']
		\coordinate (in) {};
		\node[rectangle, draw, text width=12ex, text centered, right=of in] (modell) {$y(x)$\\нелинейна};
		\coordinate[right=of modell] (out) {};
		\draw (in) -- node[above,label=below:$x$] {вход}  (modell);
		\draw (modell) -- node[above,label=below:$y$] {выход} (out);
	\end{tikzpicture}
\caption{Нелинейный элемент}
\label{fig:nel_elem}
\end{figure}
\FloatBarrier

% Принцип суперпозиции (принцип наложения) "--- допущение, согласно которому если составляющие сложного процесса воздействия взаимно не влияют друг на друга, то результирующий эффект будет представлять собой сумму эффектов, вызываемых каждым воздействием в~отдельности.

% Таким образом, для $y(x)$ есть хотя бы одна пара воздействий $x_1$ и~$x_2$, для которых этот принцип нарушен:
Таким образом, для нелинейной $y(x)$ есть хотя бы одна пара воздействий $x_1$ и~$x_2$, для которых принцип суперпозиции нарушен:
\begin{lequation}{eq:nel_func}
y(x_1 + x_2) \neq y(x_1) + y(x_2)
\end{lequation}

Система, в~которой есть хотя бы один нелинейный элемент, также является нелинейной.

\subsection{Исследование системы с~нелинейными элементами}
Системы, имеющие в~составе один, два или более нелинейных элементов, могут быть представлены различным образом:
\begin{descerate}
\item[Системы с~одним нелинейным элементом.] В~таких системах
линейные элементы объединяются в~один, а~система в~целом представляется в~виде комбинации линейного и~нелинейного элементов.
\item[Системы с~двумя нелинейными элементами]  представляются в~виде комбинации линейного четырёхполюсника с~нелинейной нагрузкой на входах и~выходах.
\item[Системы с~тремя и~более нелинейными элементами] моделируются как сугубо нелинейные.
\end{descerate}


\section{Способы задания нелинейных свойств}
\epigraph{\begin{stanza}Для чего, в самом деле, полюса, параллели,\\
    Зоны, тропики и зодиаки?\\
 И команда в ответ: <<В жизни этого нет,\\
    Это "--- чисто условные знаки.>>
\end{stanza}}
{\Snark}
Нелинейные свойства системы могут быть заданы в~различной форме:
\begin{enumerate}
\item Задание нелинейных характеристик в~виде специальных функций,
например, $e^{a \cdot x}$, $\ln (a \cdot x)$, $\sin (a \cdot x)$ и~т.\,д.
и~их~комбинаций.
\item В~виде ряда Тейлора:\nopagebreak
\begin{equation}
 f(x) = \sum_{k=0}^\infty \frac{f^{(k)} (a)}{k!} (x - a)^k,
\end{equation}
ряда Фурье:\nopagebreak
\begin{equation}
 f(x) = \frac{a_0}{2} + \sum\limits_{k=1}^{\infty} A_k\cos(2\pi \frac{k}{\tau}x+\theta_k),
\end{equation}
или полинома \nopagebreak
\begin{equation}
y = a_0  + \sum_{k=1}^n a_k x^k.
\end{equation}
% \FloatBarrier

% \pagebreak[3]
% \clearpage
\item В~табличной форме:\nopagebreak


\begin{table}[!hbt]
\caption{Таблично заданная функция $y(x)$}
\begin{tabularx}{0.8\linewidth}{|C|C|C|C|C|C|}
\hline
$x_0$ & $x_1$ & $x_2$ & $\ldots$ & $x_{n-1}$ & $x_{n}$ \\\hline
$y_0$ & $y_1$ & $y_2$ & $\ldots$ & $y_{n-1}$ & $y_{n}$ \\\hline
\end{tabularx}
\end{table}
% \FloatBarrier

% \pagebreak[3]
% \clearpage
\item В~графической форме.\nopagebreak
\begin{figure}[!ht]
% \resizebox{\approvedImageWidth}{!}{
\begin{tikzpicture}    [line cap=round,line join=round,x=8ex,y=8ex]
%%creating the ticks and xy-axis nodes
  \draw[axisline] (-0.2,0) -- node[pos=0.9, auto] {$x$} (4,0);
  \draw[axisline] (0,-.2) --  node[pos=0.9,auto] {$y$} (0,3);

  \draw[graphline,smooth,samples=100,domain=.1:4]   plot(\x,{2 - exp(\x*(-1)+.3)*(1+0.5*sin(\x*1000))});
\end{tikzpicture}
% }
\caption{Пример задания нелинейной функции $y(x)$ в~графической форме}
\label{fig:nln_graphicssetting}
\end{figure}


\end{enumerate}

\section{Аппроксимация и~интерполяция табличных функций}
\epigraph{Наглость его не знала предела, производной и~не аппроксимировалась элементарными функциями.}
{\MathFolk}
\subsection{Аппроксимация табличных функций}

Пусть $y$ является функцией аргумента $x$. Это означает, что любому значению $x$ из области определения поставлено в~соответствие значение $y$. На практике иногда невозможно записать зависимость $y(x)$ в~явном виде. Вместе с~тем, нередко эта зависимость задается в~табличном виде. Это означает, что дискретному множеству значений $\{x_i\}$ поставлено в~соответствие множество значений $\{y_i\}$, $0 < i < m$. Эти значения "--- либо результаты расчёта, либо набор экспериментальных данных.

\begin{table}[!ht]
\caption{Таблично заданная функция $y(x)$}
\begin{tabularx}{0.8\linewidth}{|C|C|C|C|C|C|}
\hline
$x_0$ & $x_1$ & $x_2$ & $\ldots$ & $x_{n-1}$ & $x_{n}$ \\\hline
$y_0$ & $y_1$ & $y_2$ & $\ldots$ & $y_{n-1}$ & $y_{n}$ \\\hline
\end{tabularx}
\end{table}
% \begin{table}[!ht]
% \caption{Таблично заданная функция $y(x)$}
% $\begin{array}{|c|c|c|c|c|c|}
% \hline
% x_0 & x_1 & x_2 & \ldots & x_{n-1} & x_{n} \\\hline
% y_0 & y_1 & y_2 & \ldots & y_{n-1} & y_{n} \\\hline
% \end{array}
% $
% \end{table}
Часто требуется найти некоторую аналитическую функцию, которая приближённо описывает заданную табличную зависимость. Кроме того, иногда требуется определить значения функции в~других точках, отличных от узловых. Этой цели служит \termin{задача о~приближении (аппроксимации).} В~этом случае находят некоторую функцию $f(x)$, такую, чтобы отклонения её от заданной табличной функции было наименьшим. Функция $f(x)$ называется \termin{аппроксимирующей}.


Вид аппроксимирующей функции  существенным образом зависит от исходной табличной функции. В~разных случаях функцию $f(x)$ выбирают в~виде экспоненциальной, логарифмической, степенной, синусоидальной и~т.\,д. В~каждом конкретном случае подбирают соответствующие параметры таким образом, чтобы достичь максимальной близости аппроксимирующей и~табличной функций.

Чаще всего функцию представляют в~виде полинома по степеням~$x$. Запишем общий вид полинома $n$-й степени:
\begin{equation}
y = a_0  + \sum_{k=1}^n a_k x^k
\end{equation}
 Коэффициенты $a_i$ подбираются таким образом, чтобы достичь наименьшего отклонения полинома от заданной функции.

Таким образом, аппроксимация "--- замена одной функции другой, близкой к~первой и~достаточно просто вычисляемой.


\subsection{Интерполяция табличных функций}

Интерполяция "--- частный случай аппроксимации.

\begin{definition}
Если для табличной функции $y=f(x)$ требуется построить аппроксимирующюю функцию $j(x)$, точно совпадающую в~узлах $x_i$ c~заданной, то такой способ называется  \termin{интерполяцией.}
\end{definition}

При интерполяции заданная функция $f(x)$ очень часто аппроксимируется с~помощью многочлена (полинома), имеющего общий вид 
\begin{equation}
j(x)=p_n(x) = a_0  + \sum_{k=1}^n a_k x^k
\end{equation}

Для данного многочлена необходимо найти коэффициенты $a_k$, такие, чтобы многочлен $p_n(x)$ при $x=x_i$ принимал значения $y_i$,
то есть:
\begin{equation}\label{eq:interpdef}
p_n(x_i)=y_i, \;  i=0,1,\ldots,n
\end{equation}

Через $n+1$ различных точек проходит \emph{единственный} полином $n$-й степени.

Существует несколько форм его записи:
\begin{itemize}[nolistsep]
\item форма Лагранжа;
\item форма Ньютона.
\end{itemize}

\subsection{Интерполяционный полином в~форме Лагранжа}
Интерполяционный полином в~форме Лагранжа $L_n(x)$ имеет вид:
\begin{equation}
L_n(x) = \sum_{j=0}^n y_j l_j(x)
\end{equation}
где $l_j(x)$ "--- \termin{базисные полиномы,} имеющие вид:
\begin{equation}
l_j(x)=\prod_{i=0, j\neq i}^{n} \frac{x-x_i}{x_j-x_i} = \frac{x-x_0}{x_j-x_0} \cdots \frac{x-x_{j-1}}{x_j-x_{j-1}} \frac{x-x_{j+1}}{x_j-x_{j+1}} \cdots \frac{x-x_{n}}{x_j-x_{n}}\,\!
\end{equation}
Базисные полиномы $l_j(x)$ обладают следующими свойствами:
\begin{equation}
\left\{
\begin{array}{rll}
l_j(x_j) &= & 1 \\
\left. l_j(x_i)\mathstrut \right|_{i \neq j} &= & 0 \\
\end{array}
\right.
\end{equation}
Таким образом, в~\termin{узлах интерполяции $x_i$} многочлен $L_n(x)$ принимает значения $y_i$.
В~точках, отличных от узлов, интерполяционный полином в~общем случае не совпадает с~заданной функцией.


\subsection{Интерполяционный полином в~форме Ньютона}

Интерполяционный полином в~форме Ньютона~$P_n(x)$ записывается в~виде: 
\begin{equation}
\begin{array}{r}
P_n(x) = c_0 + c_1(x - x_0)+ c_2 (x - x_0 )(x - x_1 ) + \ldots +\\
+ c_n (x - x_0 )(x - x_1 )\cdot\ldots\cdot(x - x_{n-1} )
\end{array}
\end{equation}
где коэффициенты $c_i$ находятся из условий~\eqref{eq:interpdef}.

Рассмотрим подробнее процесс расчёта коэффициентов~$c_i$.

% Для $i=0$
Для поиска $c_0$ рассмотрим значение полинома~$P_n(x)$ в~точке~$x_0$:
\begin{equation}
P_n(x_0) = y_0,
\end{equation}
Все слагаемые~$P_n(x)$, кроме $c_0$, в~точке~$x_0$ равны~$0$
(во все эти члены входит скобка~$(x - x_0 )$),
следовательно, $c_0=y_0$.

Предположим, что мы знаем $c_j$ для всех $j < i$.

\pagebreak[3]
По определению интерполяционного полинома:\nopagebreak
\begin{equation}
P_n(x_i) = y_i,
\end{equation}
то есть
\begin{equation}
% \begin{array}{l@{}r@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
% c_0 &+ \\
% + &c_1 &\cdot&(x_i - x_0)&&+ \\
% + &c_2 &\cdot&(x_i - x_0 )&\cdot&(x_i - x_1 )&+ \\
% + &\ldots &&+ \\
% + &c_{i-1} &\cdot&(x_i - x_0 )&\cdot&\ldots&\cdot&(x_i - x_{i-2} )& +\\
% + &c_{i} &\cdot&(x_i - x_0 )&\cdot&\multicolumn{1}{c}{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )& +\\
% + &c_{i+1} &\cdot&(x_i - x_0 )&\cdot&\multicolumn{1}{c}{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )&\cdot&(x_i - x_{i} )&+ \\
% + &\ldots &&+\\
% + &c_n  &\cdot&(x - x_0 )&\cdot&\ldots&\cdot&(x_i - x_{i} )&\cdot&\ldots&\cdot&(x - x_{n-1} )\\
% = &y_i \\
% \end{array}
% 
% 
% \hspace{-0ex}
% \begin{array}{l}
% \begin{array}{l@{~}c@{}c@{}l@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
% c_0 &+ \\
% + &c_1 &\cdot&(x_i - x_0)&&+ \\
% + &c_2 &\cdot&(x_i - x_0 )&\cdot&(x_i - x_1 )&+ \\
% + &\ldots &&+ \\
% \end{array}\\
% \begin{array}{l@{~}c@{}c@{}l@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
% % + &c_{i-1} &\cdot&(x_i - x_0 )&\cdot&{\ldots}&\cdot&(x_i - x_{i-2} )&& +\\
% % 
% + &c_{i} &\cdot&(x_i - x_0 )&\cdot&{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )&& +\\
% + &c_{i+1} &\cdot&(x_i - x_0 )&\cdot&{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )&\cdot&(x_i - x_{i} )&&+ \\
% + &\ldots &&+\\
% % 
% % + &c_n  &\cdot&(x - x_0 )&\cdot&{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )&\cdot&(x_i - x_{i} )&\cdot&\ldots&\cdot&\lefteqn{(x - x_{n-1} ) =  }\\
% % 
% \end{array} \\
% \begin{array}{l@{~}c@{}c@{}l@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
% % + &c_{i} &\cdot&(x_i - x_0 )&\cdot&{\ldots}~{\ldots}&\cdot&(x_i - x_{i-1} )&& +\\
% % + &c_{i+1} &\cdot&(x_i - x_0 )&\cdot&{\ldots}~{\ldots}&\cdot&(x_i - x_{i-1} )&\cdot&(x_i - x_{i} )&&+ \\
% % + &\ldots &&+\\
% %
% % + &c_n  &\cdot&(x - x_0 )&\cdot&{\ldots}&\cdot&(x_i - x_{i} )&\cdot&\ldots&\cdot&\lefteqn{(x - x_{n-1} ) =  }\\
% + &c_n  &\cdot&(x - x_0 )&\cdot&{\ldots}&\cdot&{(x - x_{n-1} ) =  }\\
% % % 
% = &y_i \\
% \end{array} 
% \end{array} 
% 
% 
% 
\begin{array}{l}
\begin{array}{l@{~}c@{}c@{}l@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
c_0 &+ \\
+ &c_1 &\cdot&(x_i - x_0)&&+ \\
+ &c_2 &\cdot&(x_i - x_0 )&\cdot&(x_i - x_1 )&+ \\
+ &\ldots &&+ \\
\end{array} \\
\begin{array}{l@{~}c@{}c@{}l@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
+ &c_{i-1} &\cdot&(x_i - x_0 )&\cdot&\ldots&\cdot&(x_i - x_{i-2} )& +\\
+ &c_{i} &\cdot&(x_i - x_0 )&\cdot&\multicolumn{1}{c}{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )& +\\
+ &c_{i+1} &\cdot&(x_i - x_0 )&\cdot&\multicolumn{1}{c}{\ldots}&\cdot&(x_i - x_{i-2} )&\cdot&(x_i - x_{i-1} )&\cdot & (x_i\lefteqn{ - x_{i} )+} \\
+ &\ldots &&+\\
+ &c_n  &\cdot&(x_{i} - x_0 )&\cdot&\ldots&\cdot&(x_{i} - x_{n-1} )\\
= &y_i \\
\end{array}
\end{array}
\end{equation}
Члены с~номерами, б\'{о}льшими $i$ , при $x = x_i$ равны 0. Коэффициенты $c_j$ для $j < i$ известны.
Следовательно, можно найти $c_{i}$ по формуле:
\begin{equation}
c_i = \frac{y_i \;\;-\;\; \left( \rule{0pt}{2.5ex}c_0 \;\;+\;\; \ldots \;\;+ \;\;c_{i-1} \cdot(x_i - x_0 )\cdot\ldots\cdot(x_i - x_{i-2})\right)}
{(x_i - x_0 )\cdot\ldots\cdot(x_i - x_{i-2} )\cdot(x_i - x_{i-1} )}
\end{equation}

Так как существует единственный интерполяционный полином $n$-й степени,
проходящий через $n+1$ заданных точек,
запись в~форме Ньютона и~в~форме Лагранжа даёт один и~тот же полином.
Таким образом, при приведении полиномов Ньютона и~полинома Лагранжа к~форме~$a_0 + a_1 x + a_2 x^2 + \ldots +  a_k x^k$
коэффициенты $a_k$ совпадут для обоих полиномов.

\section{Численное решение нелинейных уравнений}
\epigraph{\begin{stanza}Простые слова, их странные связи "---\\
 Какой безотказный метод!\\
 И я вижу песни, всё время одни и те же:\\
 Хочется сделать шаг.\end{stanza}}
{\Aquarium}
Общий принцип численного решения одного или нескольких нелинейных уравнений "--- \termin{принцип итерационного (последовательного) приближения}~\cite{Samar_ChMet,Heiigeman__Prikladnye_iteracionnye_metody}.
% В~начале необходимо

Итерационный цикл можно охарактеризовать четырьмя основными составляющими:
\begin{itemize}
\item начальное приближение;
\item повторяющееся действие "--- тело цикла, уточняющее текущее приближение;
\item ограничение цикла "--- условие продолжения или выхода;
\item переход к~следующей итерации.
\end{itemize}
\termin{Условием выхода} в~итерационном цикле решения уравнения должно быть условие приближения к~истинному значению корня на расстояние, меньшее заданной точности~$\varepsilon$:
\begin{equation} 
\left|
x_{n} - x^{*}
\right|
< \varepsilon
\end{equation}
где $x_{n}$ "--- приближение, полученное на $n$-м шаге итерационного цикла,\\
$x^{*}$ "--- истинное значение корня.\\
Так как истинное значение~$x^{*}$ неизвестно, на практике обычно используется следующее \termin{условие выхода:}
\begin{equation} 
\left|
x_{n} - x_{n-1}
\right|
< \varepsilon
\end{equation}
Для методов, использующих другие ограничения цикла, эти ограничения будут описаны особо.

При переходе к~следующей итерации полученное на предыдущем шаге приближение используется на следующем как начальное.

При неправильном выборе метода или начального приближения
итерационный процесс может приводить не к~тому, что текущее приближение стремится к~истинному значению корня,
а~наоборот, к~бесконечному  удалению от этого значения.
Тогда говорят, что  процесс  \termin{расходится.}

Рассмотрим наиболее популярные методы численного решения нелинейных уравнений.

\subsection{Популярные методы численного решения нелинейных уравнений}

\begin{description}
\item[Метод дихотомии] предназначен для решения уравнений вида $f(x)=0$, где $f(x)$ меняет знак в~корне.

% \begin{center} \parbox[c]{0.5\linewidth}{
% \begin{tikzpicture}    [line cap=round,line join=round,x=4ex,y=4ex]
% %%creating the ticks and xy-axis nodes
%    \foreach \x in {0,2.5,5}
%    \draw[shift={(\x,0)},thin] (0pt,1pt) -- (0pt,-1pt);
%   \draw[thin] (0,0) -- (5,0);
%   \draw[very thick] (0,0) -- (2.5,0);
% 
%   \draw[basicline,samples=2,domain=0:5] plot[ycomb,thin,mark=*] (\x,{sin((\x + 1)*180/3)});
%   \draw[smooth,samples=100,domain=0:5]   plot(\x,{sin((\x + 1)*180/3)});
%   \node[circle,draw,fill=white,inner sep=0.3ex] at (2,0) {};
% \end{tikzpicture}
% }
% \end{center}

\item[Метод простых итераций] предназначен для решения уравнений вида $\varphi(x)=x$, причём $\varphi(x)$ в~окрестности своей неподвижной точки (корня рассматриваемого уравнения) обязательно должно быть сжимающим отображением.

\item[Метод Ньютона (метод касательных)] предназначен для решения уравнений вида $f(x)=0$,
причём в~окрестности корня $f(x)$ должна быть гладкой функцией, а~её производная$f'(x)$ не должна быть равна $0$.
$$x \to x - \frac{ f(x)}{f'(x)}$$

\end{description}

Рассмотрим каждый из методов подробнее.

\subsection{Метод дихотомии (метод деления отрезка пополам)}

Метод дихотомии предназначен для решения уравнений вида
\begin{equation} 
\label{eq:nln_eq_dihot}
f(x)=0, \; x \in \mathbb{R}
\end{equation}
Начальным приближением для метода дихотомии должен быть отрезок $[a,b]$,
на котором функция $f$ имеет единственный корень,
причём нечётной кратности (то есть график $f(x)$ пересекает ось $0x$, а~не касается её).

Идея метода заключается в~том, что на каждом шаге отрезок $[a,b]$ делится пополам,
и~вычисляется значение функции~$f(x)$ на концах $a$ и~$b$ и~в~середине отрезка (обозначим её $c$).
Сравнивая знаки $f(a)$, $f(b)$ и~$f(c)$, можно узнать, на какой из половин отрезка~$f(x)$ меняет знак,
то есть  на какой из половин находится корень (рис.~\ref{fig:nln_eq_dihot}).

\begin{figure}[!ht]
\def\cc{\mathstrut\hspace{7ex}c = \frac{a+b}{2}}
\begin{tikzpicture}    [line cap=round,line join=round,x=8ex,y=8ex]
%%creating the ticks and xy-axis nodes
   \foreach \x/\name/\pos in {0/a/below,2.5/\cc/above,5/b/below}
   \draw[shift={(\x,0)},basicline] (0pt,1pt) -- node[\pos%,      outer sep=1cm
   	]{$\name\mathstrut$} (0pt,-1pt);
  \draw[basicline] (0,0) -- (5,0);
  \draw[very thick] (0,0) -- (2.5,0);

  \draw[basicline,samples=2,domain=0:5] plot[ycomb,thin,mark=*] (\x,{sin((\x + 1)*180/3)});
  \draw[graphline,smooth,samples=100,domain=0:5]   plot(\x,{sin((\x + 1)*180/3)});
%   \draw[samples=2,domain=0:0.001]   plot(\x,{sin((\x + 1)*180/3)})  coordinate (y0);
\node[circle,draw,fill=white,inner sep=0.3ex,label=below:$x^{*}$] at (2,0) {};

\end{tikzpicture}
\caption{Метод дихотомии}
\label{fig:nln_eq_dihot}
\end{figure}

Таким образом, расчётная формула метода дихотомии выглядит как
\begin{equation} 
[a,b] \to \left\{ 
\begin{array}{rlll}
{[a,c],} & f(a) \cdot f(c) &<& 0  \\
{[c,b],} & f(c) \cdot f(b) &<& 0 \\
\end{array}
\right.
\end{equation}

Если длина полученного отрезка $[a,b]$, содержащего корень, станет меньше заданной точности~$\varepsilon$,
вычисления можно завершать "--- любая точка отрезка отстоит
от истинного значения корня уравнения~\eqref{eq:nln_eq_dihot} меньше чем на~$\varepsilon$.
Обычно как итоговое приближение рассматривают середину отрезка~$\frac{a+b}{2}$,
тогда погрешность получается не более~$\frac{\varepsilon}{2}$.


Метод дихотомии имеет следующие ограничения:
\begin{itemize}
\item не обрабатывает корни чётной кратности (касание оси $0x$);
\item некорректно работает при множестве корней на отрезке $[a,b]$;
\item медленно сходится.
\end{itemize}

\subsection{Метод простых итераций}
Метод простых итераций предназначен для решения уравнений вида
\begin{equation} 
\label{eq:nln_eq_simiter_eq}
\varphi(x)=x,
\end{equation}
причём в~рассматриваемой окрестности~$U$ корня обязательно должно выполняться условие сжимаемости:
\begin{equation}
\label{eq:nln_eq_simiter_cond}
\forall x_1, x_2 \in U:
\left|
\varphi(x_{1}) - \varphi(x_{2})
\right|
\leqslant
\gamma \cdot
\left|
x_{1} - x_{2}
\right|,
% \;\; \text{где}\;
% \gamma<1
\end{equation}
где $\gamma$ "--- константа, меньшая 1.

Если условие~\eqref{eq:nln_eq_simiter_cond} выполняется не во всей окрестности корня, включающей начальное приближение, то сходимость метода простых итераций не гарантируется "--- он может сойтись, а~может разойтись. 

Для дифференцируемых функций условие~\eqref{eq:nln_eq_simiter_cond} часто заменяют на:
\begin{equation}
\label{eq:nln_eq_simiter_cond_diff}
\left| {\varphi'(x)} \right| \leqslant \gamma < 1
\;\; \forall x \in U
\end{equation}
Из~\eqref{eq:nln_eq_simiter_cond_diff} следует~\eqref{eq:nln_eq_simiter_cond}.
Действительно, согласно теореме Лагранжа:
\begin{equation} 
\begin{array}{l}
\forall x_1, x_2 \in U:\\
\exists \zeta \in [x_1, x_2] \subseteq U :  \left|
\varphi(x_1) - \varphi(x_2)
\right|
=
(x_1 - x_2) \cdot \varphi'(\zeta).
\end{array}
\end{equation}
а~из~\eqref{eq:nln_eq_simiter_cond_diff}, т.\,к. $\zeta \in U$:
\begin{equation} 
% $
\left| {\varphi'(\zeta)} \right| \leqslant \gamma < 1
% $, т.\,е.
\end{equation}
таким образом, 
условие~\eqref{eq:nln_eq_simiter_cond} выполняется.

Если уравнение не имеет вид~\eqref{eq:nln_eq_simiter_eq} или для функции~$\varphi$ не выполняется условие~\eqref{eq:nln_eq_simiter_cond}, необходимо преобразовать решаемое уравнение.

Для часто встречающихся на практике уравнений вида $f(x)=0$ часто можно применить эквивалентное преобразование вида:
\begin{equation}
\begin{array}{c}
f(x)=0\\
\Updownarrow\\
\alpha \cdot f(x) + x = x
\end{array}
\end{equation}
где $\alpha$ "--- константа, не равная нулю, которая подбирается таким образом, чтобы для полученной функции \mbox{$\varphi(x) = \alpha \cdot f(x) + x$}
в~окрестности искомого корня
выполнялось условие~\eqref{eq:nln_eq_simiter_cond}.
Иногда такое $\alpha$ невозможно подобрать, в~этом случае необходимы более сложные преобразования заданного уравнения.

Начальным приближением для метода простых итераций должна быть точка $x_0$ в~окрестности~$U$ корня (во \emph{всей} этой окрестности, включая корень, должно выполняться условие~\eqref{eq:nln_eq_simiter_cond}).


Расчёт нового приближения методом простых итераций выглядит как:
\begin{equation} 
\label{eq:nln_eq_simiter_step}
x_n = \varphi(x_{n-1})
\end{equation}

На рис.~\ref{fig:simple_iter}~а) и~в) показана последовательность шагов метода простых итераций в~случае, когда метод сходится.
На рис.~\ref{fig:simple_iter}~б) и~г) показана последовательность шагов в~случае, когда метод расходится, и~получить корень методом простых итераций не удаётся.

\begin{figure}[!h]

\newcommand{\drawaxis}{\saxyaxis (-0.2,-0.2) (4,3.8)
%   \draw[axisline] (-0.2,0) -- node[pos=0.95, auto,swap,outer sep =1mm] (xmark) {$x$} (4,0);
%   \draw[axisline] (0,-0.2) --  node[pos=0.95,auto] (ymark) {$y$} (0,3.8);
% 	\node (nullmark) at (xmark-|ymark) {$0$};
% 	\coordinate[below left=2mm of nullmark.south west] (minc);
% 	\coordinate[above right=2mm of ymark.north-|xmark.east] (maxc);
% 	
% 	\clip (minc) rectangle (maxc);

}

\newcommand{\drawzigzag}[2]{
\foreach \i/\x/\y in {#1} {
	\xpoint{\i}{\x}{\y}{_\i}{xi}	
	\coordinate (xx\i) at (\x,\x);
	
	\draw[helphelp] (xt\i) -- (xx\i);
	\draw[arrowhelp] (xx\i) -- (xf\i);
}

\foreach \i/\j in {#2} {
	\draw[arrowhelp] (xf\i) -- (xx\j);
}
}
\newcommand{\drawplots}[4]{
	\draw[graphhelp,smooth,samples=4,domain=0:3.5]   plot(\x,{\x});
	\draw[graphline] plot[smooth] file {#1};
	\drawzigzag{#2}{#3}
	\xpoint{r}{#4}{#4}{^{*}}{xroot}
	\draw[helphelp] (xtr) -- (xfr);
}
\newcommand{\minipicture}[1]{\begin{tikzpicture}[line cap=round,line join=round,%scale=1.2,
% ,transform shape fit работает, но масштабируется текст!
node distance = 3mm]
#1
\end{tikzpicture}
}

% \resizebox{1\linewidth}{!}
{
\begin{tabular}{cc}
\minipicture{	\drawaxis
	\drawplots{plots/sitera.mat}{0/0.5/1.5, 1/1.5/2.46, 2/2.46/2.93002}{0/1, 1/2}{3}
	\node[above = of xf1] {$y=\varphi(x)$};
	\node[anchor=south west] at  ([shift=(10:3mm)] xfr) {$y=x$};
}
&
\minipicture{	\drawaxis
	\drawplots{plots/siterb.mat}{0/1.5/2.06, 1/2.06/2.92554, 2/2.92554/3.96718}{0/1, 1/2}{0.83333}
	\node[left=  of xf1] {$y=\varphi(x)$};
	\node[right=of xx2] {$y=x$};
}

\\[-2ex]
a) & б) \\
\minipicture{	\drawaxis
	\drawplots{plots/siterv.mat}{0/0.3/1.98164, 1/1.98164/0.775687, 2/0.775687/1.42077, 3/1.42077/0.983054}{0/1, 1/2, 2/3}{1.1398}
	\node[anchor=south west] at  ([shift=(-15:7mm)]xf1) {$y=\varphi(x)$};
	\node at (2.5, 3) {$y=x$};
}
&
\minipicture{	\drawaxis
	\drawplots{plots/siterg.mat}{0/1.2/3.19972, 1/3.19972/0.86547, 2/0.86547/4.27227}{0/1, 1/2}{1.8748}
	\node[anchor= west] at  ([shift=(100:4mm)]xf0) {$y=\varphi(x)$};
	\node[left= 0mm of xx2]  {$y=x$};
}

\\[-2ex]
в) & г) \\
\end{tabular}}
\caption{Последовательные шаги метода простых итераций}
\label{fig:simple_iter}
\end{figure}

Рассмотрим расстояние между новым приближением~$x_n$ и~корнем~$x^{*}$.
В~силу того, что $x^{*}$ "--- корень уравнения~\eqref{eq:nln_eq_simiter_eq} (то есть $x^{*} = \varphi(x^{*})$),
а~также расчётной формулы~\eqref{eq:nln_eq_simiter_step} получаем:
\begin{equation} 
\left|
x_{n} - x^{*}
\right|
=
\left|
\varphi(x_{n-1}) - \varphi(x^{*})
\right|
\end{equation}
с~учётом~\eqref{eq:nln_eq_simiter_cond}:
\begin{equation} 
\left|
x_{n} - x^{*}
\right|
\leqslant
\gamma\cdot 
\left|
x_{n-1} - x^{*}
\right|
\end{equation}
То есть новое приближение $x_{n}$ ближе к~корню, чем предыдущее $x_{n-1}$:
\begin{equation} 
\left|
x_{n} - x^{*}
\right|
<
\left|
x_{n-1} - x^{*}
\right|
\end{equation}
Скорость сходимости метода простых итераций 
определяется величиной~$\gamma$.

Если начальное приближение достаточно близко к~корню~$x^{*}$, можно сказать, что скорость сходимости определяется величиной производной в~корне $|\varphi'(x^{*})|$ "--- чем эта величина меньше, тем быстрее точки из окрестности~$x^{*}$ <<притягиваются>> к~корню $x^{*}$. Если $|\varphi'(x^{*})|= 0$, корень называется \termin{сверхпритягивающим} и~скорость сходимости в~его окрестности максимальна.

\subsection{Метод Ньютона} 

Метод Ньютона (метод касательных) предназначен для решения уравнений вида
\begin{equation} 
\label{eq:nln_eq_newton_eq}
f(x)=0
\end{equation}
и~является развитием метода простых итераций.
Для реализации метода необходимо преобразовать~\eqref{eq:nln_eq_newton_eq} к~виду~\eqref{eq:nln_eq_simiter_eq}
так, чтобы все корни~\eqref{eq:nln_eq_newton_eq} стали сверхпритягивающими.
Для этого рассмотрим уравнение вида
\begin{equation}
\label{eq:nln_eq_newton_eq_mod}
\alpha(x) \cdot f(x) + x = x
\end{equation}
при $\alpha(x) \neq 0$ это уравнение эквивалентно~\eqref{eq:nln_eq_newton_eq}.

Для того, чтобы любой корень~$x^*$ уравнения~\eqref{eq:nln_eq_newton_eq_mod} был сверхпритягивающим, необходимо
\begin{equation}
\left.
(\alpha(x) \cdot f(x) + x)'
\right|_{x^*}
= 0
\end{equation}
отсюда получаем
\begin{equation}\label{eq:nln_eq_newton_over}
\alpha'(x^*) \cdot f(x^*) +\alpha(x^*) \cdot f'(x^*)  + 1
= 0
\end{equation}
так как $x^*$ "--- корень, $f(x^*) = 0$, и~первое слагаемое левой части~\eqref{eq:nln_eq_newton_over} также равно~$0$.
Соответственно,~\eqref{eq:nln_eq_newton_over} эквивалентно уравнению: 
\begin{equation}
\alpha(x^*) \cdot f'(x^*)  + 1
= 0
\end{equation}
то есть
\begin{equation}
\alpha(x^*) = - \frac{1}{f'(x^*)}
\end{equation}
если производная $f'(x^*)$ ограничена, то $\alpha(x^*) \neq 0$, то есть преобразование уравнений допустимо.

Если производная $f'(x)$ ограничена во всей рассматриваемой области, то можно выбрать
% $\alpha(x) = - \frac{1}{f'(x)}$.
\begin{equation}
\alpha(x) = - \frac{1}{f'(x)}.
\end{equation}

\pagebreak[3]
Таким образом, расчёт нового приближения %методом Ньютона 
выполняется по формуле:
\begin{equation} 
\label{eq:nln_eq_newton_step}
x_n = x_{n-1} - \frac{ f(x_{n-1})}{f'(x_{n-1})}
\end{equation}

Геометрическая интерпретация метода Ньютона "--- построение касательной к~графику функции $f(x)$ в~точке текущего приближения и~расчёт точки пересечения этой касательной с~осью абсцисс.
Эта точка и~берётся в~качестве следующего приближения.

Процесс продолжается, пока не будет достигнута необходимая точность.


На рис.~\ref{fig:newton} показана последовательность шагов метода Ньютона.
\begin{figure}[!h]
% \includegraphics[width=\approvedImageWidth]{image822_newton}
\begin{tikzpicture}[line cap=round,line join=round,%scale=1.2,
scale=0.9,
% ,transform shape fit работает, но масштабируется текст!
node distance = 3mm]
	\saxyaxis (-0.2,-0.3) (4.2,4)
% 	\draw[graphline] plot[smooth] file {plots/newton.mat};
	\draw[graphline,smooth,samples=8,domain=0.2:4]   plot(\x,{exp(\x-2)-0.5});
	
	\foreach \i/\x/\y in {0/3.5/3.98169, 1/2.61157/1.34331, 2/1.88282/0.389421}{
		\xpoint{\i}{\x}{\y}{_\i}{xi}	
		\draw[helphelp] (xt\i) -- (xf\i);
	}
	\foreach \i/\j in {0/1, 1/2} {
		\draw[graphhelp] (xf\i) -- (xt\j);
	}	
	\xpoint{r}{1.3069}{0}{^{*}}{xroot}
	\node[anchor=east] at  ([shift=(180:1ex)]barycentric cs:xf0=1,xf1=1) {$y=f(x)$};
\end{tikzpicture}
\caption{Последовательные шаги метода Ньютона}
\label{fig:newton}
\end{figure}


В~точках, где $f'(x) \to \infty$, функция $\alpha(x) = -\frac{ 1}{f'(x)}$ обращается в~нуль,
и~уравнение~\eqref{eq:nln_eq_newton_eq_mod} обращается в~верное равенство.
Однако, <<новые>> корни уравнения~\eqref{eq:nln_eq_newton_eq_mod} не будут притягивающими (производная левой части в~такой точке равна 1, то есть точка нейтральна).

Метод Ньютона позволяет быстро рассчитать корень уравнения с~высокой точностью,
но для его применения необходимо знать аналитическое выражение для производной~$f'(x)$.


\endgroup
